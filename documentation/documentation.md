# Documentation #
OpenApi/Swagger was used to document the backend endpoints. The page will take longer (around 30 seconds) to load the first time someone views it in a while. Link: https://experis-mefit-backend.herokuapp.com/swagger-ui.html  
<br>
Live deployment of project. The page will take longer (around 30 seconds) to load the first time someone views it in a while. It is recommended to first open the OpenApi/Swagger link, so the backend application is ready to be loaded when the frontend application starts. Link: https://experis-mefit.herokuapp.com/
<br>
<br>
Figma was used for creating wireframes to model the frontend application. Link: https://www.figma.com/file/V0Ee2Opji4oMXFJ8ZktRKV/Case-Wireframe
<br>
<br>
DatabaseERDiagram.png - Picture of the entity relationship diagram of the backend database. Location: documentation/DatabaseERDiagram.png
<br>
<br>
USER MANUAL.pdf - Description of the functionalities of the application. Location: documentation/USER MANUAL.pdf


