# MEFIT - Hiba, Sarah, Maren og Renze. #
<br>

## Installation guide ##
1. Download or clone the project and open it in your preferred editor. This guide will assume Intellij is used.

2. Set url, password and username for the spring datasource in src/main/resources/application.properties.

3. Edit the environment variables in the run configuration to include clientsecret and clientid with correct variables.
    - In Intellij, open Run -> Edit Configurations... Then click on Environment and add clientid=CLIENTID;clientsecret=CLIENTSECRET to Environment variables.
    <br>

4. Run MeFitApplication with the configurations from step 3.
    - Optional: After spring is done setting up, run createDummyData.sql in src/main/java/com.experis.mefit/sqlQueries to fill up the database with premade data.
    <br>
5. In the terminal window, navigate to src/main/react/reactjs. Run npm install. 
6. When the terminal window finishes installing, run npm start. This will start the front-end application.
