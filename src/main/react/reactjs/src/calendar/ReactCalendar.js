import Calendar from 'react-calendar';
import React from 'react';
import { useState } from 'react';
import 'react-calendar/dist/Calendar.css';
import './ReactCalendar.css';

function ReactCalendar(props) {
    const [value, setValue] = useState(new Date());
    localStorage.setItem('chosenDate', value);

    const onChange = value => {
        setValue(value);
        localStorage.setItem('chosenDate', value);
        props.updateParent();
        props.handleClick();
    };

    return (
        <div className="calendarBox">
            <div className="calendar">
                <button className="calendarBtn" type="button" onClick={props.handleClick}>x</button>
                <Calendar onChange={onChange} value={value} />
            </div>
        </div>
    );
}

export default ReactCalendar;
