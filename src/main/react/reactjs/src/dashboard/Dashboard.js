import CurrentGoals from './CurrentGoals';
import Dates from './Dates';
import './Dashboard.css';
//calender(dagens dato)
//progress-bar
//box som sier  (dager som er igjen) av målet
//button set goal, redirect to traningCatalogue page
//<viewgoal> - ukentlige mål - clickbare(redirect to viewgoal)

import React, { useState } from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import AllGoals from './AllGoals';

function Dashboard() {
    const { isAuthenticated } = useAuth0();
    const [reRender, setReRender] = useState(false);

    const updateChild = () => {
        setReRender(!reRender);
    };

    const updateDashboard = () => {
        setReRender(!reRender);
    };

    return (
        isAuthenticated && (
            <div className="dashboard">
                <h1> Goals for the week</h1>
                <div className="Periode__kalender">
                    <Dates updateDashboard={updateDashboard} />
                </div>

                <div className="dashContent">
                    <CurrentGoals reRender={reRender} />

                    <div className="achievedGoals">
                        <h3>Previous Goals</h3>
                        <ul className="goalList">
                            <AllGoals
                                reRender={reRender}
                                updateComponent={updateChild}
                            />
                        </ul>
                    </div>
                </div>
            </div>
        )
    );
}

export default Dashboard;
