import { useAuth0 } from "@auth0/auth0-react";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import ProgressDates from "./ProgressDates";
import './CurrentGoals.css';
import {apiAudience, apiLink, fetchFromDatabase} from "../sharedComponents/ApiService";

function CurrentGoals(props) {

    const {getAccessTokenSilently} = useAuth0();

    const history = useHistory();

    const [goalData, setGoalData] = useState([]);
    const [progress, setProgress ] = useState(0);
    const [workoutDetails, setWorkoutDetails] = useState(false);

    const currentId = localStorage.getItem('currentId');
    const startDate = localStorage.getItem('StartDate');
    const endDate = localStorage.getItem('EndDate');

    const currentDate = new Date();
    const dayOfWeek = currentDate.getDay();

    const loadGoalDetails= e => {
        history.push({
            pathname: '/goaldetails',
            goalId: e.target.id
        })
    };

    const redirectAddGoals = () => {
        history.push('/trainingcatalogue')
    }

    const checkAchieved = e => {
        return goalData[e].achieved === true;
    }

    const toggleWorkouts = () => {
        if (workoutDetails){
            setWorkoutDetails(false);
        } else {
            setWorkoutDetails(true);
        }
    }

    useEffect(() => {
        let mounted = true;
        if (mounted) {
            getGoalsFromDB();
        }
        return () => mounted = false;
    }, [props.reRender]);

    const getGoalsFromDB = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json',
                StartDate: `${startDate}`,
                EndDate: `${endDate}`
            }
        }, `${apiLink}profile/${currentId}/goals/between-dates`)
            .then(data => {
                setGoalData(data)
                if (data) {
                    let countGoals = data.length;
                    let countAchieved = 0;
                    for (let item of data) {
                        if (item.achieved) {
                            countAchieved++
                        }
                    }
                    if (countAchieved !== 0) {
                        setProgress(countAchieved * (100 / countGoals));
                    }
                }else {
                    setProgress(0);
                }
            })
    }

    const updateGoal = async (targetId, targetChecked) => {
        await fetchFromDatabase({
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                goal_id: targetId,
                achieved: `${targetChecked}`,
                end_date: `${endDate}`,
                start_date: `${startDate}`
            })
        }, apiLink + 'goal')
    }

    const calculateProgress = e => {
        let countGoals = 0;
        let value = e.target.checked;

        if (goalData){ //If userGoals are defined
            countGoals = goalData.length; //Count how many goals in userGoals array in profileData
        }

        if(value === true){ //If checkbox has been checked, add progress to progressbar.
            setProgress(progress+(100/countGoals)); //100% divided by count of goals gives percentage one goal adds to progress when checked
            //Update goal achieved = true
        } else {
            setProgress(progress-(100/countGoals)); //Removes progress if checkbox is unchecked
            //Update goal achieved = false
        }
        updateGoal(e.target.value, e.target.checked);
    }

    return (
        <div className="currentGoals">
            <div className="goalsLeft">
                {!goalData || goalData.length === 0 &&
                <div className="noGoals">
                    <h2>You don't have any current goals</h2>
                    <button onClick={redirectAddGoals}>Add goals</button>
                </div>
                }
                {goalData && //Renders when userGoals have been defined
                        <ul className="goalList">
                            {goalData.length !== 0 &&
                                <button onClick={toggleWorkouts}>Expand goals</button>
                            }

                            {goalData.map((item, index) => { //Prints list of Goals with checkboxes
                                return <li className="goalItems" key={index}>
                                    <div className="goalAndCheck">
                                        <label key="label" id={item.goal_id} onClick={loadGoalDetails}>{item.name}</label>
                                        <input key="checkBox" type="checkbox" value={item.goal_id} onClick={calculateProgress} defaultChecked={checkAchieved(index)}/>
                                    </div>
                                    <div>
                                        {item.goalWorkouts && workoutDetails &&
                                            <ul className="workoutsInGoal">
                                                {item.goalWorkouts.map((item, i) => {
                                                    return <li key={i}>
                                                        {item.name}
                                                    </li>
                                                })}
                                            </ul>
                                        }
                                    </div>
                                </li>
                            })}
                        </ul>
                }
            </div>

            <div className="progressRight">
                <ProgressDates reRender={props.reRender} progress={progress} currentDate={currentDate} dayOfWeek={dayOfWeek} />
            </div>
            </div>
    )
}

export default CurrentGoals
