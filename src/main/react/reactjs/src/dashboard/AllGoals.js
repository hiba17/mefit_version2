import { useAuth0 } from "@auth0/auth0-react";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import './CurrentGoals.css';
import {apiLink, fetchFromDatabase} from "../sharedComponents/ApiService";

function AllGoals(props) {

    const {getAccessTokenSilently} = useAuth0();

    const apiAudience = 'me-fit-endpoint.com';
    const history = useHistory();

    const [goalData, setGoalData] = useState([]);

    const currentId = localStorage.getItem('currentId');
    const startDate = localStorage.getItem('StartDate');
    const endDate = localStorage.getItem('EndDate');

    const loadGoalDetails= e => {
        history.push({
            pathname: '/goaldetails',
            goalId: e.target.id
        })
    };

    const updateGoalDate = async e => {
        await fetchFromDatabase({
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                goal_id: e.target.id,
                achieved: false,
                end_date: `${endDate}`,
                start_date: `${startDate}`
            })
        }, apiLink + 'goal')
        props.updateComponent()
    }

    useEffect(() => {
        let mounted = true;
        if (mounted) {
            getAllGoalsFromDB();
        }
        return () => mounted = false;
    }, [props.reRender]);

    const getAllGoalsFromDB = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, `${apiLink}profile/${currentId}/goals`)
            .then(response => setGoalData(response)
        )
    }



    return (
        <>
            {goalData && //Renders when userGoals have been defined
                <div className="prevGoals">
                    {goalData.map((item, index) => { //Prints list of Goals with checkboxes
                        return <ul key={index}>
                            {item.start_date < startDate && item.start_date !== startDate &&
                                <span key="span">
                                    <li key="listItem" id={item.goal_id} onClick={loadGoalDetails}>{item.name}</li>
                                    <button key="addButton" onClick={updateGoalDate} id={item.goal_id} type="button"> Add</button>
                                </span>
                            }
                        </ul>
                    })}
                </div>
            }
        </>
    )
}

export default AllGoals
