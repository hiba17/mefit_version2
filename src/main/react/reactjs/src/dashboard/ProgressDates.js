import React, {useEffect} from 'react'
import './ProgressDates.css'

function ProgressDates(props) {
    let numberDay;

    let styles = {
        strokeDasharray: 440,
        strokeDashoffset: `calc(440 - (440 * ${props.progress}) / 100)`,
        stroke: '#53bc83'
    }

    useEffect(() => {
    }, [props.reRender])

    if (props.dayOfWeek === 0){
        numberDay = 7;
    } else {
        numberDay = props.dayOfWeek;
    }

    return(
        <>
            <h2> DAY {numberDay} / 7 </h2>
            <h3> {7 - numberDay} days left! </h3>

            <div className="percentContainer">
                <div className="percent">
                    <svg className="svgCircle">
                        <circle cx="70" cy="70" r="70" />
                        <circle cx="70" cy="70" r="70" style={styles} />
                    </svg>
                    <div className="number">
                        <h2>{Math.round(props.progress)}%</h2>
                        <h6>Completed</h6>
                    </div>
                </div>
            </div>

        </>
    )
}
export default ProgressDates
