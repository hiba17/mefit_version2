import React, { useState } from 'react';
import ReactCalendar from '../calendar/ReactCalendar';

function Dates(props) {
    const [currentDate, setCurrentDate] = useState(new Date());
    const dayOfWeek = currentDate.getDay();
    const [click, setClick] = useState(false);
    const handleClick = () => {
        setClick(!click);
    };

    const updateParent = () => {
        setCurrentDate(new Date(localStorage.getItem('chosenDate')))
        props.updateDashboard();
    }

    const calcMonday =
        currentDate.getDate() - dayOfWeek + (dayOfWeek === 0 ? -6 : 1);
    let startDate = new Date(currentDate.setDate(calcMonday));
    let endDate = new Date().setDate(startDate.getDate() + 6);

    let tzoffset = (new Date()).getTimezoneOffset() * 60000;

    endDate = new Date(endDate- tzoffset).toISOString().slice(0, 10);
    startDate = new Date(startDate - tzoffset).toISOString().slice(0, 10);


    localStorage.setItem('StartDate', startDate);
    localStorage.setItem('EndDate', endDate);

    return (
        <div className="calendarAndBtn">
            <h3>
                Period: {startDate} → {endDate}
            </h3>
            <button className="calendarButton" onClick={handleClick}>Change week</button>
            {click && <ReactCalendar handleClick={handleClick} updateParent={updateParent}/>}
        </div>
    );
}
export default Dates;
