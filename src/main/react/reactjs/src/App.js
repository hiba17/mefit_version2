import './App.css';
import Navbar from  "./navbar/Navbar.js";
import Login from  "./login/Login.js";
import Dashboard from  "./dashboard/Dashboard.js";
import GoalDetails from  "./goals/viewgoal/GoalDetails.js";
import TrainingCatalogue from  "./catalogue/TrainingCatalogue.js";
import Profile from  "./profile/Profile.js";
import ContributorsArea from  "./contributorsArea/ContributorsArea.js";
import CreateProfile from "./createProfile/profileForm/CreateProfile.js";
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import LoginRedirect from "./login/LoginRedirect";
import {useAuth0} from "@auth0/auth0-react";
import Loading from "./sharedComponents/Loading";
import AssignContributor from "./assignContributor/AssignContributor";

function App(){
  const { isLoading } = useAuth0;

  if (isLoading) {
    return <Loading />;
  }
  return (

    <div className="App">
      
        <Router>
          <div className="App">   
        <Switch>

        <Route path="/dashboard">
          <Navbar/>
          <Dashboard/>
        </Route>

          <Route path="/goaldetails">
            <Navbar/>
            <GoalDetails/>
          </Route>

          <Route path="/trainingcatalogue">
            <Navbar/>
            <TrainingCatalogue/>
          </Route>
          
          <Route path="/profile">
            <Navbar/>
            <Profile/>
          </Route>

          <Route path="/contributorsarea">
            <Navbar/>
            <ContributorsArea/>
          </Route>

          <Route path="/assigncontributor">
            <Navbar/>
            <AssignContributor/>
          </Route>

          <Route path="/usercreation">
            <CreateProfile/>
          </Route>

          <Route path="/loginredirect">
            <Navbar />
            <LoginRedirect />
          </Route>

          <Route path="/">
            <Login/>
          </Route>



        </Switch>
      
      </div>


    </Router>


    </div>


  )

}

export default App;
