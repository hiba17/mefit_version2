import ExampleLoginButton from "../components/ExampleAuth0Buttons/ExampleLoginButton";
import './Login.css'

//brukernavn

//passord


//auth -- sikkerhet
//registration button (redirect registration page)
//login button (redirect to dashboard)

import React from 'react'
import {useAuth0} from "@auth0/auth0-react";
import LoginRedirect from "./LoginRedirect";


function Login() {
    const {isAuthenticated} = useAuth0();

    if (isAuthenticated) {
        return <LoginRedirect/>
    }
    else return (
        <div className="login">
            <div className="loginContent">
                <h1>MeFit </h1>
                <h3>Your fitness journey starts now</h3>
                <div className="buttonContainer">
                    <ExampleLoginButton/>
                </div>
            </div>
        </div>
    )
}

export default Login
