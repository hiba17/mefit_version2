import React from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import { useHistory } from 'react-router-dom';
import { useEffect } from 'react';
import { apiLink, fetchFromDatabase } from '../sharedComponents/ApiService';

function LoginRedirect() {
    const { user, isAuthenticated, getAccessTokenSilently } = useAuth0();
    const apiAudience = 'me-fit-endpoint.com';
    const history = useHistory();

    const getIdFromDB = async () => {
        fetchFromDatabase(
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${await getAccessTokenSilently({
                        audience: apiAudience,
                    })}`,
                    Email: user.email,
                },
            },
            apiLink + 'profile'
        ).then((data) => {
            if (data !== undefined) {
                localStorage.setItem('currentId', data.profileId);
                history.push('/dashboard');
            } else {
                history.push('/usercreation');
            }
        });
    };

    useEffect(() => {
        if (localStorage.getItem('currentId')){
            history.push('/dashboard')
        } else {
            getIdFromDB();
        }
    }, [isAuthenticated]);


    return <></>;
}

export default LoginRedirect;
