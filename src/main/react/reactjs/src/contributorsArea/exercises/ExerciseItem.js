import DisplayTraining from "../../catalogue/displayComponents/DisplayTraining";
import React from "react";
import {apiAudience, apiLink, fetchFromDatabase} from "../../sharedComponents/ApiService";
import {useAuth0} from "@auth0/auth0-react";

export function ExerciseItem(props) {
    const {getAccessTokenSilently} = useAuth0()

    const deleteExercise = async () => {
        await fetchFromDatabase({
            method: 'DELETE',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, apiLink + 'exercise/' + props.item.exercise_id)
        props.updateValues()
    }
    console.log(props)
    return (
        <div className='item-list'>
            <div className='item-list-left'>
                <DisplayTraining item={props.item} name={'Exercise'} addGoalButton={false}/>
            </div>
            <div className='item-list-right'>
                <button className='delete-button' onClick={deleteExercise}>Delete exercise</button>
            </div>
        </div>
    )
}
