import * as Yup from "yup";
import {Formik} from "formik";
import {apiAudience, apiLink, fetchFromDatabase} from "../../sharedComponents/ApiService";
import {useAuth0} from "@auth0/auth0-react";

export function CreateExercise(props) {
    const {getAccessTokenSilently} = useAuth0();
    const createExercise = async (values) => {
        await fetchFromDatabase({
            method: 'POST',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(values)
        }, apiLink + 'exercise')
    }

    const SignupSchema = Yup.object().shape({
        name: Yup.string()
            .min(2, "Too Short!")
            .max(255, "Too long")
            .required("Required"),
        description: Yup.string()
            .min(2, "Too Short!")
            .max(255, "Too long")
            .required("Required"),
        target_muscle_group: Yup.string()
            .min(2, "Too Short!")
            .max(255, "Too long")
            .required("Required"),
        image_link: Yup.string()
            .min(2, "Too Short!")
            .max(255, "Too long")
            .required("Required"),
        vid_link: Yup.string()
            .min(2, "Too Short!")
            .max(255, "Too long")
            .required("Required"),
    });
    return (
        <div>
            <Formik
                initialValues={{
                    name: '',
                    description: '',
                    target_muscle_group: '',
                    image_link: '',
                    vid_link: ''
                }}
                validationSchema={SignupSchema}
                onSubmit={(values, {setSubmitting}) => {
                    values.contributor = {profile_id: Number(localStorage.getItem('currentId'))}
                    createExercise(values).then(r => {
                        props.updateValues()
                    });
                }}
            >
                {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleSubmit,

                  }) => (
                    <form onSubmit={handleSubmit}>
                        <h2>Create exercise</h2>
                        <span><label> Exercise name: </label>
                    <input
                        type="text"
                        name="name"
                        onChange={handleChange}
                        value={values.name}/>
                            {errors.name && touched.name && <div>{errors.name}</div>}
                    </span>
                        <span><label> Description: </label>
                    <input
                        type="text"
                        name="description"
                        value={values.description}
                        onChange={handleChange}/>
                            {errors.description && touched.description && <div>{errors.description}</div>}
                    </span>
                        <span><label>Target Muscle Group: </label>
                    <input type="text" name="target_muscle_group" value={values.target_muscle_group}
                           onChange={handleChange}/>
                    </span>
                        <span><label> Video link: </label>
                    <input
                        type="text"
                        name="vid_link"
                        value={values.vid_link}
                        onChange={handleChange}/>
                            {errors.vid_link && touched.vid_link && <div>{errors.vid_link}</div>}
                    </span>
                        <span><label> Image link: </label>
                    <input
                        type="text"
                        name="image_link"
                        value={values.image_link}
                        onChange={handleChange}/>
                            {errors.image_link && touched.image_link && <div>{errors.image_link}</div>}
                    </span>
                        <button className="form-input-btn" type="submit">Save</button>
                    </form>
                )}
            </Formik>
        </div>
    )
}

