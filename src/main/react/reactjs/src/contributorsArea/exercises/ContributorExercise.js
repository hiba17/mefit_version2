import React from 'react'
import {apiAudience, apiLink, fetchFromDatabase} from "../../sharedComponents/ApiService";
import {useState, useEffect} from "react";
import {useAuth0} from "@auth0/auth0-react";
import '../ContributorArea.css';
import {CreateExercise} from "./CreateExercise";
import {ExerciseItem} from "./ExerciseItem";

function ContributorExercise() {
    const {getAccessTokenSilently} = useAuth0();
    const [exercises, setExercises] = useState(undefined)

    const getExercises = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, apiLink + 'exercise')
            .then(response => {
                setExercises(response)
            })
    }

    useEffect(() => {
        getExercises()
    }, []);


    if (exercises === undefined) {
        return (<div></div>)
    }
    const myExercises = []
    exercises.map((item, index) => {
        if (Number(localStorage.getItem('currentId')) === item.contributor)
            myExercises.push(item)
    })
    return (
        <div className='contributor-area'>
            <div className="left box-item">
                <h2> Your Contributed Exercises</h2>
                {myExercises.map((item, index) => (
                    <ExerciseItem key={index} item={item} updateValues={getExercises}/>
                ))}
            </div>
            <div className="right box-item">
                <CreateExercise updateValues={getExercises}/>
            </div>
        </div>
    )
}

export default ContributorExercise
