import React from 'react';

function ContributorNavbar(props) {
    const sendDataWorkout = (e) => {
        props.data(1);
    };
    const sendDataExercise = (e) => {
        props.data(2);
    };
    const sendDataProgram = (e) => {
        props.data(3);
    };
    return (
        <div className="navbarContributor">
            <button onClick={sendDataProgram}>Programs</button>
            <button onClick={sendDataWorkout}>Workouts</button>
            <button onClick={sendDataExercise}>Exercises</button>
        </div>
    );
}

export default ContributorNavbar;
