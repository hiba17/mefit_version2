import {useAuth0} from "@auth0/auth0-react";
import {apiAudience, apiLink, fetchFromDatabase} from "../../sharedComponents/ApiService";
import * as Yup from "yup";
import {Formik} from "formik";
import {AddableExercise} from "../workouts/AddableExercise";
import React, {useEffect, useState} from "react";
import {AddableWorkout} from "./AddableWorkout";

export function CreateProgram(props) {
    const {getAccessTokenSilently} = useAuth0();
    const [workouts, setWorkouts] = useState([]);
    const [selectedWorkouts, setSelectedWorkouts] = useState([]);

    useEffect(() => {
        getAllWorkouts()
    }, []);

    const createProgram = async (values) => {
        await fetchFromDatabase({
            method: 'POST',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(values)
        }, apiLink + 'program')
    }

    const addWorkout = (workout_id) => {
        setSelectedWorkouts(workouts => [...workouts, {
            workout_id: workout_id
        }])
    }

    const removeWorkout = (workout_id) => {
        setSelectedWorkouts(selectedWorkouts => selectedWorkouts.filter(item => item.workout_id !== workout_id))
    }

    const getAllWorkouts = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`
            }
        }, apiLink + 'workout').then(data => {
            setWorkouts(data)
        })
    }

    const SignupSchema = Yup.object().shape({
        name: Yup.string()
            .min(2, "Too Short!")
            .max(255, "Too long")
            .required("Required"),
        category: Yup.string()
            .min(2, "Too Short!")
            .max(255, "Too long")
            .required("Required"),
        image_link: Yup.string()
            .min(2, "Too Short!")
            .max(255, "Too long")
            .required("Required"),

    });
    return (
        <div>
            <Formik
                initialValues={{
                    name: '',
                    category: '',
                    image_link: '',
                    difficulty_level: 'low'
                }}
                validationSchema={SignupSchema}
                onSubmit={(values, {setSubmitting}) => {
                    values.contributor_profile = {profile_id: Number(localStorage.getItem('currentId'))}
                    values.workoutList = selectedWorkouts
                    createProgram(values).then(r => {
                        props.updateValues()
                    });
                }}
            >
                {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleSubmit,

                  }) => (
                    <form onSubmit={handleSubmit}>
                        <h2>Create program</h2>
                        <span><label> Program name: </label>
                    <input
                        type="text"
                        name="name"
                        onChange={handleChange}
                        value={values.name}/>
                            {errors.name && touched.name && <div>{errors.name}</div>}
                    </span>
                        <span>
                            <label> Category: </label>
                    <input
                        type="text"
                        name="category"
                        value={values.category}
                        onChange={handleChange}/>
                            {errors.category && touched.category && <div>{errors.category}</div>}
                    </span>
                        <span>
                            <label> Link to image: </label>
                    <input
                        type="text"
                        name="image_link"
                        value={values.image_link}
                        onChange={handleChange}/>
                            {errors.image_link && touched.image_link && <div>{errors.image_link}</div>}
                    </span>
                        <span>
                        <label> Difficulty level: </label>
                        <select
                            name="difficulty_level"
                            onChange={handleChange}
                            defaultValue={values.difficulty_level}>
                            <option value="low">low</option>
                            <option value="medium">medium</option>
                            <option value="high">high</option>
                        </select>
                            {errors.difficulty_level && touched.difficulty_level &&
                            <div>{errors.difficulty_level}</div>}
                    </span>
                        <span className='workout-list'>
                            {workouts.map((item, index) => {
                                return (
                                    <div key={index}>
                                        <AddableWorkout item={item}
                                                        added={selectedWorkouts.find(workout => workout.workout_id === item.workout_id) !== undefined}
                                                        add={addWorkout}
                                                        remove={removeWorkout}/>
                                    </div>
                                )
                            })}
                        </span>
                        <button className="form-input-btn" type="submit">Save</button>
                    </form>
                )}
            </Formik>
        </div>
    )
}

