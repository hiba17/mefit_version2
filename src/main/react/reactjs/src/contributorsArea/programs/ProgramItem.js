import {useAuth0} from "@auth0/auth0-react";
import {apiAudience, apiLink, fetchFromDatabase} from "../../sharedComponents/ApiService";
import DisplayTraining from "../../catalogue/displayComponents/DisplayTraining";
import React from "react";

export function ProgramItem(props) {
    const {getAccessTokenSilently} = useAuth0()

    const deleteProgram = async () => {
        await fetchFromDatabase({
            method: 'DELETE',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, apiLink + 'program/' + props.item.program_id)
        props.updateValues()
    }

    return (
        <div className='item-list'>
            <div className='item-list-left'>
                <DisplayTraining item={props.item} name={'Program'} addGoalButton={false}/>
            </div>
            <div className='item-list-right'>
                <button className='delete-button' onClick={deleteProgram}>Delete program</button>
            </div>
        </div>
    )
}
