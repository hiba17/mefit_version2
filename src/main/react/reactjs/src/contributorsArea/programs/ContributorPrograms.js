import React, {useEffect, useState} from 'react'
import FetchAndCreateList from '../../catalogue/FetchAndCreateList'
import {useAuth0} from "@auth0/auth0-react";
import {apiAudience, apiLink, fetchFromDatabase} from "../../sharedComponents/ApiService";
import {ExerciseItem} from "../exercises/ExerciseItem";
import {CreateExercise} from "../exercises/CreateExercise";
import {ProgramItem} from "./ProgramItem";
import {CreateProgram} from "./CreateProgram";

function ContributorPrograms() {
    const {getAccessTokenSilently} = useAuth0();
    const [programs, setPrograms] = useState(undefined)

    const getPrograms = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, apiLink + 'program')
            .then(response => {
                setPrograms(response)
            })
    }

    useEffect(() => {
        getPrograms()
    }, []);


    if (programs === undefined) {
        return (<div></div>)
    }

    const myPrograms = []
    programs.map(item => {
        if (Number(localStorage.getItem('currentId')) === item.contributor_profile)
            myPrograms.push(item)
    })
    return (
        <div className='contributor-area'>
            <div className="left box-item">
                <h2> Your Contributed Programs</h2>
                {myPrograms.map(item => (
                    <ProgramItem item={item} updateValues={getPrograms}/>
                ))}
            </div>
            <div className="right box-item">
                <CreateProgram updateValues={getPrograms}/>
            </div>
        </div>
    )
}
export default ContributorPrograms
