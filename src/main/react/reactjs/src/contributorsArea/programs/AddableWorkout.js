import React, {useState} from "react";
import DisplayTraining from "../../catalogue/displayComponents/DisplayTraining";

export function AddableWorkout(props) {

    const addWorkout = () => {
        props.add(props.item.workout_id)
    }

    const removeWorkout = () => {
        props.remove(props.item.workout_id)
    }

    return (<div className='program-creator'>
        <div className='display-training'><DisplayTraining item={props.item} name={'Workout'} addGoalButton={false}/></div>
        {!props.added && <button type='button' onClick={addWorkout}>Add</button>}
        {props.added && <button type='button' onClick={removeWorkout}>Remove</button>}
    </div>)
}
