import React, {useState} from 'react'
import ContributorExercise from './exercises/ContributorExercise'
import ContributorPrograms from './programs/ContributorPrograms'
import ContributorsWorkout from './workouts/ContributorWorkout'
import FilterNavbar from "../catalogue/FilterNavbar";
import {useAuth0} from "@auth0/auth0-react";
import {useHistory} from "react-router-dom";

function ContributorsArea() {
    const {user} = useAuth0();
    const history = useHistory();

    let isContributor = false;
    if (user) {
        isContributor = user['https://me-fit-endpoint.com/roles'].includes("contributor")
    }

    if(!isContributor){
        history.push('./dashboard')
    }

    const [dataType, setDataType] = useState(1);
    // 1 - workout, 2 - exercise, 3 - program
    const configureDataType = (number) => {
        setDataType(number)
    }

    return (
        <div className='contributor-main'>
            <FilterNavbar data={configureDataType}/>
            <h1 className='main-title'>Contributors Area</h1>
            <div className="contributor-content">
            {dataType === 1 && <ContributorsWorkout/>}
            {dataType === 2 && <ContributorExercise/>}
            {dataType === 3 && <ContributorPrograms/>}
            </div>
        </div>
    )
}

export default ContributorsArea
