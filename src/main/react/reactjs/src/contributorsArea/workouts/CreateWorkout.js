import {useAuth0} from "@auth0/auth0-react";
import {apiAudience, apiLink, fetchFromDatabase} from "../../sharedComponents/ApiService";
import * as Yup from "yup";
import {Formik} from "formik";
import React, {useEffect} from "react";
import {useState} from "react";
import {AddableExercise} from "./AddableExercise";

export function CreateWorkout(props) {
    const {getAccessTokenSilently} = useAuth0();
    const [exercises, setExercises] = useState([]);
    const [selectedExercises, setSelectedExercises] = useState([]);

    useEffect(() => {
        getAllExercises()
    }, []);

    const addExercise = (exercise_id, repetitions) => {
        setSelectedExercises(exercises => [...exercises, {
            exercise: {exercise_id: exercise_id},
            exercise_repetitions: repetitions
        }])
    }

    const removeExercise = (exercise_id) => {
        setSelectedExercises(selectedExercises => selectedExercises.filter(item => item.exercise.exercise_id !== exercise_id))
    }

    const createWorkout = async (values) => {
        await fetchFromDatabase({
            method: 'POST',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(values)
        }, apiLink + 'workout')
    }

    const getAllExercises = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`
            }
        }, apiLink + 'exercise').then(data => {
            setExercises(data)
        })
    }

    const SignupSchema = Yup.object().shape({
        name: Yup.string()
            .min(2, "Too Short!")
            .max(255, "Too long")
            .required("Required"),
        type: Yup.string()
            .min(2, "Too Short!")
            .max(255, "Too long")
            .required("Required"),
        image_link: Yup.string()
            .min(2, "Too Short!")
            .max(255, "Too long")
            .required("Required"),
        time: Yup.string()
            .matches('^[0-9]+$', 'Invalid time')
            .required('Required'),
    });

    if (exercises === undefined) {
        return (<div>LOADING</div>)
    }

    return (
        <div>
            <Formik
                initialValues={{
                    name: '',
                    type: '',
                    image_link: '',
                    time: '',
                    difficulty_level: 'low'
                }}
                validationSchema={SignupSchema}
                onSubmit={(values, {setSubmitting}) => {
                    values.contributorProfile_id = Number(localStorage.getItem('currentId'))
                    values.exercises = selectedExercises
                    createWorkout(values).then(r => {
                        props.updateValues()
                    });
                }}
            >
                {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleSubmit,

                  }) => (
                    <form onSubmit={handleSubmit}>
                        <h2>Create workout</h2>
                        <span><label> Workout name: </label>
                    <input
                        type="text"
                        name="name"
                        onChange={handleChange}
                        value={values.name}/>
                            {errors.name && touched.name && <div>{errors.name}</div>}
                    </span>
                        <span><label> Type: </label>
                    <input
                        type="text"
                        name="type"
                        value={values.type}
                        onChange={handleChange}/>
                            {errors.type && touched.type && <div>{errors.type}</div>}
                    </span>
                        <span><label> Image link: </label>
                    <input
                        type="text"
                        name="image_link"
                        value={values.image_link}
                        onChange={handleChange}/>
                            {errors.image_link && touched.image_link && <div>{errors.image_link}</div>}
                    </span>
                        <span>
                        <label> Difficulty level: </label>
                        <select
                            name="difficulty_level"
                            onChange={handleChange}
                            defaultValue={values.difficulty_level}>
                            <option value="low">low</option>
                            <option value="medium">medium</option>
                            <option value="high">high</option>
                        </select>
                            {errors.difficulty_level && touched.difficulty_level &&
                            <div>{errors.difficulty_level}</div>}
                    </span>
                        <span><label> Time (in minutes): </label>
                    <input
                        type="text"
                        name="time"
                        value={values.time}
                        onChange={handleChange}/>
                            {errors.time && touched.time && <div>{errors.time}</div>}
                    </span>
                        <span className='exercise-list'>
                            {exercises.map((item, index) => {
                                return (
                                    <div key={index}>
                                        <AddableExercise item={item}
                                                         added={selectedExercises.find(exercise => exercise.exercise.exercise_id === item.exercise_id) !== undefined}
                                                         add={addExercise}
                                                         remove={removeExercise}/>
                                    </div>
                                )
                            })}
                        </span>
                        <button className="form-input-btn" type="submit">Save</button>
                    </form>
                )}
            </Formik>

        </div>
    )
}

