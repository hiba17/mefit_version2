//FetchAndCreateList component (added by contributor)
//edit & delete button  (for every item in sharedlist)
//sharedList(med exercises)-(add to workout button)

import React, {useEffect, useState} from 'react'
import FetchAndCreateList from '../../catalogue/FetchAndCreateList'
import {apiAudience, apiLink, fetchFromDatabase} from "../../sharedComponents/ApiService";
import {useAuth0} from "@auth0/auth0-react";
import {ExerciseItem} from "../exercises/ExerciseItem";
import {CreateExercise} from "../exercises/CreateExercise";
import {WorkoutItem} from "./WorkoutItem";
import {CreateWorkout} from "./CreateWorkout";

function ContributorsWorkout() {
    const {getAccessTokenSilently} = useAuth0();
    const [workouts, setWorkouts] = useState(undefined)

    const getWorkouts = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, apiLink + 'workout')
            .then(response => {
                setWorkouts(response)
            })
    }
    useEffect(() => {
        getWorkouts()
    }, []);


    if (workouts === undefined) {
        return (<div>LOADING</div>)
    }

    const myWorkouts = []
    workouts.map((item, index) => {
        if (Number(localStorage.getItem('currentId')) === item.contributorProfile_id)
            myWorkouts.push(item)
    })

    return (
        <div className='contributor-area'>
            <div className="left box-item">
                <h2> Your Contributed Workouts</h2>
                {myWorkouts.map((item, index) => (
                    <WorkoutItem item={item} key={index} updateValues={getWorkouts}/>
                ))}
            </div>
            <div className="right box-item">
                <CreateWorkout updateValues={getWorkouts}/>
            </div>
        </div>
    )
}

export default ContributorsWorkout
