import {useAuth0} from "@auth0/auth0-react";
import {apiAudience, apiLink, fetchFromDatabase} from "../../sharedComponents/ApiService";
import DisplayTraining from "../../catalogue/displayComponents/DisplayTraining";
import React from "react";

export function WorkoutItem(props) {
    const {getAccessTokenSilently} = useAuth0()

    const deleteWorkout = async () => {
        await fetchFromDatabase({
            method: 'DELETE',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, apiLink + 'workout/' + props.item.workout_id)
        props.updateValues()
    }
    return (
        <div className='item-list'>
            <div className='item-list-left'>
                <DisplayTraining item={props.item} name={'Workout'} addGoalButton={false}/>
            </div>
            <div className='item-list-right'>
                <button className='delete-button' onClick={deleteWorkout}>Delete workout</button>
            </div>
        </div>
    )
}
