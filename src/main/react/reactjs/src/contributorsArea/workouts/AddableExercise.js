import DisplayTraining from "../../catalogue/displayComponents/DisplayTraining";
import React from "react";
import {useState} from "react";

export function AddableExercise(props) {

    const [number, setNumber] = useState('1')

    const addExercise = () => {
        props.add(props.item.exercise_id, number)
    }

    const removeExercise = () => {
        props.remove(props.item.exercise_id)
    }

    return (<div className="trainingList">
        <DisplayTraining item={props.item} name={'Exercise'} addGoalButton={false}/>
        <input className="setValue" id='number' type='number' defaultValue='1' min='1' onChange={event => setNumber(event.target.value)}></input>
        {!props.added && <button type='button' onClick={addExercise}>Add</button>}
        {props.added && <button type='button' onClick={removeExercise}>Remove</button>}
    </div>)

}
