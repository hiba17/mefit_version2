export async function fetchFromDatabase(requestOptions, apiLink) {
    // Fetch from the database with the apiUrl and requestOptions
    return fetch(apiLink, requestOptions)
        .then(response => {
            console.log(response)
            return response
        })
        .then(response => response.json())
        .then(data => {
            return data
        })
        .catch(error => console.log(error))
}

export const apiAudience = 'me-fit-endpoint.com';

// Production: https://experis-mefit-backend.herokuapp.com/api/v1/
// Developement: http://localhost:8080/api/v1/
//export const apiLink = 'https://experis-mefit-backend.herokuapp.com/api/v1/'
//export const simpleApiLink = 'https://experis-mefit-backend.herokuapp.com'
export const apiLink = 'http://localhost:8080/api/v1/'
export const simpleApiLink = 'http://localhost:8080'



