//validating form input

export default function validateInfo(formstate) {
    let errors = {};

    //firstname
    if (!formstate.first_name.trim()) {
        errors.first_name = 'First name is required';
    }

    //lastname
    if (!formstate.last_name.trim()) {
        errors.last_name = 'Last name is required';
    }

    //weight
    if (!formstate.weight.trim()) {
        errors.weight = 'weight is required';
    }

    //height
    if (!formstate.height.trim()) {
        errors.height = 'height is required';
    }

    if (!formstate.fitness_evaluation.trim()) {
        errors.fitness_evaluation = 'Fitness level is required';
    }
}
