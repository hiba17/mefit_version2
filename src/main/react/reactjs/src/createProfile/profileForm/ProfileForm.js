import './ProfileForm.css';
import { useAuth0 } from '@auth0/auth0-react';
//import validateInfo from "./validateInfo";
import { Formik } from 'formik';
import * as Yup from 'yup';
import {
    apiAudience,
    apiLink,
    fetchFromDatabase,
} from '../../sharedComponents/ApiService';
import { useHistory } from 'react-router-dom';

function ProfileForm() {
    const history = useHistory();
    const { user, isAuthenticated, getAccessTokenSilently } = useAuth0();

    const getProfileFromEmail = async (email) => {
        return await fetchFromDatabase(
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${await getAccessTokenSilently({
                        audience: apiAudience,
                    })}`,
                    Email: email,
                },
            },
            apiLink + 'profile'
        );
    };

    const setUserToAuthUser = async (profileId) => {
        fetchFromDatabase(
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${await getAccessTokenSilently({
                        audience: apiAudience,
                    })}`,
                },
            },
            apiLink + 'profile/' + profileId + '/set-to-user'
        );
    };

    const SignupSchema = Yup.object().shape({
        first_name: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too long')
            .required('Required'),
        last_name: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too long')
            .required('Required'),
        weight: Yup.string()
            .matches('^[0-9]+$', 'Invalid weight')
            .required('Required'),
        height: Yup.string()
            .matches('^[0-9]+$', 'Invalid height')
            .required('Required'),
        fitness_evaluation: Yup.string().required('Required'),
    });

    // user: Holds information about the logged in user
    // isAuthenticated: Boolean to check if user is authenticated
    // getAccessTokenSilently: Gets JWL token from the Auth0 server, if necessary

    const createProfile = async (values) => {
        fetchFromDatabase(
            {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${await getAccessTokenSilently({
                        audience: apiAudience,
                    })}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(values),
            },
            apiLink + 'profile'
        ).then((response) => {
            getProfileFromEmail(values.email).then((data) =>
                setUserToAuthUser(data.profileId)
            );
        });
    };

    return (
        isAuthenticated && (
            <div className="inputbox">
                <Formik
                    initialValues={{
                        email: user.email,
                        first_name: '',
                        last_name: '',
                        weight: '',
                        height: '',
                        fitness_evaluation: '',
                    }}
                    validationSchema={SignupSchema}
                    onSubmit={(values, { setSubmitting }) => {
                        console.log(values);
                        createProfile(values).then((r) => {
                            history.push('/loginredirect');
                        });
                    }}
                >
                    {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleSubmit,
                    }) => (
                        <form onSubmit={handleSubmit}>
                            <p className="headlineCreateUser">Create user</p>
                            <img
                                className="profileImage"
                                src="https://expertphotography.com/wp-content/uploads/2020/08/profile-photos-2.jpg"
                            />
                            <div className="personalInfoCreateProfile">
                                <p className="headlinePersonalInformation">
                                    Personal Information
                                </p>
                                <span>
                                    <label> First name </label>
                                    <input
                                        type="text"
                                        name="first_name"
                                        onChange={handleChange}
                                        value={values.first_name}
                                    />
                                    {errors.first_name &&
                                        touched.first_name && (
                                            <div>{errors.first_name}</div>
                                        )}
                                </span>
                                <span>
                                    <label> Last name </label>
                                    <input
                                        type="text"
                                        name="last_name"
                                        value={values.last_name}
                                        onChange={handleChange}
                                    />
                                    {errors.last_name && touched.last_name && (
                                        <div>{errors.last_name}</div>
                                    )}
                                </span>
                                <span>
                                    <label> Email </label>
                                    <input
                                        type="text"
                                        id="disabled"
                                        name="email"
                                        value={user.name}
                                        disabled
                                    />
                                </span>
                            </div>
                            <div className="fitnessInfoCreateProfile">
                                <p className="headlinePersonalInformation">
                                    Fitness Information
                                </p>
                                <span>
                                    <label> Weight </label>
                                    <input
                                        type="text"
                                        name="weight"
                                        value={values.weight}
                                        onChange={handleChange}
                                    />
                                    {errors.weight && touched.weight && (
                                        <div>{errors.weight}</div>
                                    )}
                                </span>
                                <span>
                                    <label> Height </label>
                                    <input
                                        type="text"
                                        name="height"
                                        value={values.height}
                                        onChange={handleChange}
                                    />
                                    {errors.height && touched.height && (
                                        <div>{errors.height}</div>
                                    )}
                                </span>
                                <span>
                                    <label> Fitness Level </label>
                                    <select
                                        onChange={handleChange}
                                        name="fitness_evaluation"
                                        defaultValue="Level"
                                    >
                                        <option
                                            selected
                                            disabled
                                            defaultValue="Level"
                                        >
                                            Level
                                        </option>
                                        <option value="Low">Low</option>
                                        <option value="Medium">Medium</option>
                                        <option value="High">High</option>
                                    </select>
                                    {errors.fitness_evaluation &&
                                        touched.fitness_evaluation && (
                                            <div>
                                                {errors.fitness_evaluation}
                                            </div>
                                        )}
                                </span>
                            </div>
                            <div className="checkbox">
                                <div className="checkbox1">
                                    <label>Add two factor authentication</label>
                                    <input
                                        type="checkbox"
                                        id="twoFactorAuth"
                                        name="twoFactorAuth"
                                        value="twoFactorAuth"
                                    />
                                </div>
                                <div className="checkbox2">
                                    <label> Request contributor rights </label>
                                    <input
                                        type="checkbox"
                                        id="requestContributor"
                                        name="requestContributor"
                                        value="requestContributor"
                                    />
                                </div>
                            </div>
                            <button className="saveProfileButton" type="submit">
                                Save
                            </button>
                        </form>
                    )}
                </Formik>
            </div>
        )
    );
}

export default ProfileForm;
