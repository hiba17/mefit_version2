import { CSSTransition } from 'react-transition-group';
import Burger from '@animated-burgers/burger-squeeze';
import '@animated-burgers/burger-squeeze/dist/styles.css';
import ExampleLogoutButton from '../components/ExampleAuth0Buttons/ExampleLogoutButton';
import React from 'react';
import {
    Nav,
    NavLink,
    Bars,
    NavMenu,
    FitLogo,
    MobileMenu,
} from './NavbarElement';
import { useAuth0, withAuthenticationRequired } from '@auth0/auth0-react';
import Loading from '../sharedComponents/Loading';
import { useState } from 'react';

function Navbar() {
    const {user} = useAuth0();
    const isContributor = user['https://me-fit-endpoint.com/roles'].includes("contributor")
    const isAdmin = user['https://me-fit-endpoint.com/roles'].includes("admin")
    const [click, setClick] = useState(false);
    const handleClick = () => setClick(!click);

    return (
        <Nav>
            <div className="menu-icon" onClick={handleClick}>
                <NavLink to="/">
                    <FitLogo>
                        <span className="meFit">MeFit</span>
                    </FitLogo>
                </NavLink>
                <Bars>
                    <Burger onClick={handleClick} isOpen={click} />
                </Bars>
                <CSSTransition
                    in={click}
                    unmountOnExit
                    timeout={200}
                    classNames="mobile-menu"
                >
                    <MobileMenu>
                        <NavLink to="/dashboard" onClick={handleClick}>
                            Home
                        </NavLink>
                        <NavLink to="/trainingcatalogue" onClick={handleClick}>
                            Training Catalogue
                        </NavLink>
                        {isContributor && (
                            <NavLink
                                to="/contributorsarea"
                                onClick={handleClick}
                            >
                                Contributor
                            </NavLink>
                        )}
                        {isAdmin && (
                            <NavLink
                                to="/assigncontributor"
                                onClick={handleClick}
                            >
                                Admin Page
                            </NavLink>
                        )}
                        <NavLink to="/profile" onClick={handleClick}>
                            Profile
                        </NavLink>
                        <ExampleLogoutButton />
                    </MobileMenu>
                </CSSTransition>
            </div>
            <NavMenu>
                <NavLink to="/dashboard">Home</NavLink>
                <NavLink to="/trainingcatalogue">Training Catalogue</NavLink>
                {isContributor && (
                    <NavLink to="/contributorsarea">Contributor</NavLink>
                )}
                {isAdmin && (
                    <NavLink to="/assigncontributor">Admin Page</NavLink>
                )}
                <NavLink to="/profile">Profile</NavLink>
                <ExampleLogoutButton />
            </NavMenu>
        </Nav>
    );
}
//export default Navbar

//This is what makes the private route work:/
export default withAuthenticationRequired(Navbar, {
    onRedirecting: () => <Loading />,
});
