import styled, { keyframes } from 'styled-components';
import { NavLink as Link } from 'react-router-dom';
import { FaBars, FaTimes } from 'react-icons/all';

const slideIn = keyframes`
   from {
     bottom: 100%;
   }
   to {
      bottom: 0;
   }
`;

const slideOut = keyframes`
   from {
     bottom: 0;
   }
   to {
      bottom: 100%;
   }
`;

export const Nav = styled.nav`
    background: white;
    height: 60px;
    display: flex;
    justify-content: space-between;
    padding-bottom: 12px;
    z-index: 10;
    font-size: 13px;
`;

export const NavLink = styled(Link)`
    color: black;
    display: flex;
    align-items: center;
    text-decoration: none;
    padding: 0 1rem;
    height 100%;
    cursor: pointer;
    border-bottom: 3px solid transparent;
    
    @media screen and (max-width: 768px){
       margin-left: 20px;
       width:50%;
    
      
     
    }
    
    &.active {
        color: #0d8490;
         
    }
    &:after {
         background: none repeat scroll 0 0 transparent;
         bottom: 0;
         content: "";
         display: block;
         height: 2px;
         left: 50%;
         position: absolute;
         background: black;
         transition: width 0.3s ease 0s, left 0.3s ease 0s;
         width: 0;
     
    }
    
    &:hover:after{
        width: 100%; 
        height: 1px;
         
    }
    
     @media screen and (max-width: 768){
        display: none;
     
        
    }
   
`;

export const LogOutButton = styled.button`
    font-size: 13px;
    font-family: Roboto, sans-serif;
    text-transform: uppercase;
    border: 1.3px solid black;
    color: black;
    display: flex;
    align-items: center;
    text-decoration: none;
    height 50%;
    cursor: pointer;
    border-radius: 4px;
    background: white;
    padding: 15px 27px;
    outline: none;
    cursor: pointer
    translation: all 0.2s ease-in-out;
    text-decoration:none;
    display: flex;
    align-items: center;
    margin-right: 24px;
    
    margin-left:24px;
    &.active {
        color: #0d8490;
    }
    
    
    &:hover{
        transition: all 0.2s ease-in-out;
        background: #0d8490;
        border-color:transparent;
        color: white;
    }
    
    
    @media screen and (max-width: 768px){
        padding: 8px 19px;
        margin: 25px 6px 49px 24px;
 
    }

`;

export const Bars = styled.div`
    display: none;
    font-color: black;

    .burger .burger-lines,
    .burger .burger-lines:after,
    .burger .burger-lines:before {
        background-color: black;
    }

    &:hover:after {
        .burger .burger-lines,
        .burger .burger-lines:after,
        .burger .burger-lines:before {
            background-color: black;
        }
    }

    &.active {
        .burger .burger-lines,
        .burger .burger-lines:after,
        .burger .burger-lines:before {
            background-color: black;
        }
    }
    @media screen and (max-width: 768px) {
        display: block;
        position: absolute;
        z-index: 2;
        top: 0;
        right: 0;
        transform: translate(-100%, 75%);
        font-size: 1.8rem;
        cursor: pointer;
    }
`;

export const Cross = styled(FaTimes)`
    display: none;
    color: black;

    @media screen and (max-width: 768px) {
        display: block;
        position: absolute;
        top: 0;
        right: 0;
        transform: translate(-100%, 75%);
        font-size: 1.8rem;
        cursor: pointer;
    }
`;

export const NavMenu = styled.div`
    display: flex;
    align-items: center;
    /*margin-right: -24px;*/

    margin-right: 24px;
    @media screen and (max-width: 768px) {
        display: none;
    }
`;

export const NavBtn = styled.nav`
    display: flex;
    align-items: center;
    margin-right: 24px;

    @media screen and (max-width: 768) {
        display: none;
    }
`;

export const NavBtnLink = styled(Link)`
    border-radius: 4px;
    background: #0d8490;
    padding: 10px 22px;
    color: #fff;
    border:none;
    outline: none;
    cursor: pointer
    translation: all 0.2s ease-in-out;
    text-decoration:none;
    
    margin-left:24px;
    
    &:hover{
        transition: all 0.2s ease-in-out;
        background: #fff;
        color: #010606;

    }
`;

export const FitLogo = styled.span`
    .meFit {
        font-family: 'Exo 2', sans-serif;
        font-weight: 600;
        font-size: 7vh;
        text-transform: none;
        color: black;
    }

    @media screen and (max-width: 768px) {
        margin-top: 7px;
    }
`;

export const MobileMenu = styled.nav`
    position: fixed;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: white;
    display: flex;
    flex-direction: column;
    z-index: 1;
    transition: all 10s ease;
    padding-top: 40px;
    overflow: hidden;

    @media screen and (min-width: 769px) {
        display: none;
    }

    &.mobile-menu-enter {
        animation: ${slideIn} 0.3s;
    }
    &.mobile-menu-active {
    }
    &.mobile-menu-exit {
        animation: ${slideOut} 0.3s;
    }
    &.mobile-menu-exit-active {
    }
`;
