// epost + fitness attributes + other details(bilde etc..)
// request contributor
//edit profile (button) able
//save changes button

//update password(auth?)

//add 2 faktor authentication

// epost + fitness attributes + other details(bilde etc..)
// request contributor
//edit profile (button) able
//save changes button

//update password(auth?)

//add 2 faktor authentication
import './EditProfile.js'
import './Profile.css'
import React from "react";
import {useAuth0} from "@auth0/auth0-react";
import {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import EditProfile from "./EditProfile";
import {apiAudience, apiLink, fetchFromDatabase} from "../sharedComponents/ApiService";


function Profile() {
    // user: Holds information about the logged in user
    // isAuthenticated: Boolean to check if user is authenticated
    // getAccessTokenSilently: Gets JWL token from the Auth0 server, if necessary
    const { user, isAuthenticated, getAccessTokenSilently } = useAuth0();
    const apiAudience = 'me-fit-endpoint.com';
    const [profileData, setProfileData] = useState({});
    const history = useHistory();
    const currentId = localStorage.getItem('currentId');
    const [showEdit, setShowEdit] = useState(false);
    let isContributor;
    let isAdmin;
    if (user) {
        isContributor = user['https://me-fit-endpoint.com/roles'].includes("contributor")
        isAdmin = user['https://me-fit-endpoint.com/roles'].includes("admin")
    }

    useEffect(() => {
        getProfileFromDatabase();
    }, []);

    const getProfileFromDatabase = async () => {
        fetchFromDatabase({
            method: 'GET',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience,})}`
            }
        }, apiLink + "profile/" + currentId)
            .then(data => setProfileData(data))
            .catch(error => console.log(error))
    };

    const handleSubmitted = () => {
        setShowEdit(false);
        getProfileFromDatabase();
    };

    const requestContributor = async () => {
        fetchFromDatabase(
            {
                method: 'PUT',
                headers: {
                    Authorization: `Bearer ${await getAccessTokenSilently({
                        audience: apiAudience,
                    })}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    profile_id: profileData.profile_id,
                    email: profileData.email,
                    first_name: profileData.first_name,
                    last_name: profileData.last_name,
                    fitness_evaluation: profileData.fitness_evaluation,
                    height: parseInt(profileData.height),
                    weight: parseInt(profileData.weight),
                    contributor: true,
                }),
            },
            apiLink + 'profile'
        ).then(getProfileFromDatabase);
    };

    return (
        isAuthenticated && (
            <div className="profilePage">
                <div className="profile">
                    <div className="infoProfile">
                        <img
                            className="profileImage"
                            src="https://expertphotography.com/wp-content/uploads/2020/08/profile-photos-2.jpg"
                        />
                        <p className="headline name">
                            {profileData.first_name} {profileData.last_name}
                        </p>
                        <span>
                            <p> Email: </p>
                            <p>{profileData.email}</p>
                        </span>
                        <button className="requestTwoFactButton">
                            <img
                                className="twoFactLogo"
                                src="/logo/authenticator.png"
                            />
                            Add two factor authentication
                        </button>
                        {!isAdmin &&
                            !isContributor &&
                            !profileData.contributor && (
                                <button
                                    className="requestConButton"
                                    onClick={requestContributor}
                                >
                                    <img
                                        className="penLogo"
                                        src="/logo/pen.png"
                                    />
                                    Request contributor rights
                                </button>
                            )}
                    </div>
                    <div className="workoutInfoProfile">
                        <p className="headline">Fitness Info</p>
                        <img id="logoWeight" src="/logo/weight.png" />
                        {!showEdit && (
                            <div className="fitnessInfo">
                                <span>
                                    <p className="information"> Weight: </p>
                                    <p className="informationData">
                                        {profileData.weight}
                                    </p>
                                </span>
                                <span>
                                    <p className="information"> Height: </p>
                                    <p className="informationData">
                                        {' '}
                                        {profileData.height}
                                    </p>
                                </span>
                                <span>
                                    <p className="information">
                                        {' '}
                                        Fitness Evaluation:{' '}
                                    </p>
                                    <p className="informationData">
                                        {profileData.fitness_evaluation}
                                    </p>
                                </span>
                                <span id="editButton">
                                    <button
                                        className="editProfile"
                                        onClick={() => setShowEdit(true)}
                                    >
                                        Change profile
                                    </button>
                                </span>
                            </div>
                        )}
                        {showEdit && (
                            <EditProfile
                                user={profileData}
                                onCancel={() => setShowEdit(false)}
                                onSubmitted={handleSubmitted}
                            />
                        )}
                    </div>
                </div>
            </div>
        )
    );
}

export default Profile;
