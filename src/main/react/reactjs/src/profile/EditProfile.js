import './EditProfile.css'
import React from "react";
import {useAuth0} from "@auth0/auth0-react";
import {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import * as Yup from "yup";
import {Formik} from "formik";
import {apiLink, fetchFromDatabase} from "../sharedComponents/ApiService";

function EditProfile(props) {
    console.log(props.user);

    const SignupSchema = Yup.object().shape({
        weight: Yup.string()
            .matches('^[0-9]+$', 'Invalid weight')
            .required('Required'),
        height: Yup.string()
            .matches('^[0-9]+$', 'Invalid height')
            .required('Required'),
        fitness_evaluation: Yup.string().required('Required'),
    });
    // user: Holds information about the logged in user
    // isAuthenticated: Boolean to check if user is authenticated
    // getAccessTokenSilently: Gets JWL token from the Auth0 server, if necessary
    const {isAuthenticated, getAccessTokenSilently} = useAuth0();
    const apiAudience = 'me-fit-endpoint.com';
    const [profileData, setProfileData] = useState({});
    const currentId = localStorage.getItem('currentId');

    const changeProfile = async (values) => {
        await fetchFromDatabase(
            {
                method: 'PUT',
                headers: {
                    Authorization: `Bearer ${await getAccessTokenSilently({
                        audience: apiAudience,
                    })}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    profile_id: currentId,
                    email: props.user.email,
                    first_name: props.user.first_name,
                    last_name: props.user.last_name,
                    fitness_evaluation: values.fitness_evaluation,
                    height: parseInt(values.height),
                    weight: parseInt(values.weight),
                }),
            }, `${apiLink}profile`)
    }

    return (
        <div className="fitnessInfo">
            <p className="name">
                {profileData.first_name} {profileData.last_name}
            </p>
            <Formik
                initialValues={{
                    weight: props.user.weight,
                    height: props.user.height,
                    fitness_evaluation: props.user.fitness_evaluation,
                }}
                validationSchema={SignupSchema}
                onSubmit={(values, {setSubmitting}) => {
                    console.log(values);
                    changeProfile(values).then((r) => {
                        props.onSubmitted();
                    });
                }}
            >
                {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      isSubmitting,
                  }) => (
                    <form onSubmit={handleSubmit}>
                        <span>
                            <label> Weight: </label>
                            <input
                                type="text"
                                name="weight"
                                onChange={handleChange}
                                value={values.weight}
                            />
                            {errors.weight && touched.weight && (
                                <div>{errors.weight}</div>
                            )}
                        </span>
                        <span>
                            <label> Height: </label>
                            <input
                                type="text"
                                name="height"
                                onChange={handleChange}
                                value={values.height}
                            />
                            {errors.height && touched.height && (
                                <div>{errors.height}</div>
                            )}
                        </span>
                        <span>
                            <label> Fitness Evaluation: </label>
                            <select
                                name="fitness_evaluation"
                                onChange={handleChange}
                                defaultValue={values.fitness_evaluation}
                            >
                                <option value="Low">Low</option>
                                <option value="Medium">Medium</option>
                                <option value="High">High</option>
                            </select>
                            {errors.fitness_evaluation &&
                            touched.fitness_evaluation && (
                                <div>{errors.fitness_evaluation}</div>
                            )}
                        </span>
                        <span>
                            <button id="saveButton" type="submit">
                                Save
                            </button>
                            <button id="cancelButton" onClick={props.onCancel}>
                                Cancel
                            </button>
                        </span>
                    </form>
                )}
            </Formik>
        </div>
    );
}

export default EditProfile;
