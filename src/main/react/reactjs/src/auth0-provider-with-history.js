import React from "react";
import { useHistory } from "react-router-dom";
import { Auth0Provider } from "@auth0/auth0-react";

const Auth0ProviderWithHistory = ({ children }) => {
    const history = useHistory();
    const domain = 'dev-mefit.eu.auth0.com';
    const clientId = 'JVmLQlaOjuwyo5Nw32PK4YPUD9eFKqqc';

    const onRedirectCallback = (appsState) => {
        history.push(appsState?.returnTo || window.location.pathname)
    };

    return (
        <Auth0Provider
            domain={domain}
            clientId={clientId}
            redirectUri={window.location.origin}
            onRedirectCallback={onRedirectCallback}
            audience='me-fit-endpoint.com'
            >
            {children}
        </Auth0Provider>
    );
};

export default Auth0ProviderWithHistory;
