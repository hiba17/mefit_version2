import React from "react";
import {apiAudience, apiLink, fetchFromDatabase} from "../../sharedComponents/ApiService";
import {useAuth0} from "@auth0/auth0-react";
import {useEffect} from "react";

export function GoalWorkoutComplete(props) {
    const {getAccessTokenSilently} = useAuth0();

    useEffect(()=> {
    }, [props.workout.complete]);

    const completeWorkout = async () => {
        await fetchFromDatabase({
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                workout: {
                    workout_id: props.workout.workout.workout_id
                },
                complete: !props.workout.complete
            })
        }, apiLink + 'goal/' + props.goal_id + '/add-workout')
        props.recalculateGoal()
        props.workout.complete = !props.workout.complete
    }

    if (!props.workout.complete) {
        return (
            <div>
                <img src=''/>
                <input type='checkbox' onChange={completeWorkout}/>
            </div>
        )
    } else {
        return (
            <div>
                <input type='checkbox' onChange={completeWorkout} checked/>
            </div>
        )
    }
}
