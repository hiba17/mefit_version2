import IconClose from '../../sharedComponents/icons/IconClose';

export function GoalComplete(props) {
    if (props.achieved) {
        console.log('displaying');
        return (
            <img src={process.env.PUBLIC_URL + '/check.png'} alt="Checked" />
        );
    } else {
        return <IconClose className="uncheck" color="#1078B2" width="100px" />;
    }
}
