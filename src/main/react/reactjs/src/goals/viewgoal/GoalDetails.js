import { useHistory } from 'react-router-dom';

//display alle workout , de som er completed i en liste og de andre i en annen liste

//perioden

//button til previuos achived goals

//status(progress bar)

//back button

import React from 'react';
import {
    apiAudience,
    apiLink,
    fetchFromDatabase,
} from '../../sharedComponents/ApiService';
import { useAuth0 } from '@auth0/auth0-react';
import { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import './GoalDetails.css';
import { GoalWorkoutComplete } from './GoalWorkoutComplete';
import { GoalComplete } from './GoalComplete';
import DisplayTraining from '../../catalogue/displayComponents/DisplayTraining';

function GoalDetails(props) {
    const { getAccessTokenSilently } = useAuth0();
    const location = useLocation();
    let goal_id = location.goalId;

    const history = useHistory();
    const loadDashboard = (e) => {
        history.push('/dashboard');
    };

    const [goal, setGoal] = useState(undefined);
    const getGoal = async () => {
        await fetchFromDatabase(
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${await getAccessTokenSilently({
                        audience: apiAudience,
                    })}`,
                },
            },
            apiLink + 'goal/' + goal_id
        ).then((response) => {
            response.goalWorkouts.sort(function (a, b) {
                return a.goalWorkout_id - b.goalWorkout_id;
            });
            setGoal(response);
        });
    };

    useEffect(() => {
        getGoal();
    }, []);

    if (goal === undefined) {
        return <div>LOADING</div>;
    }

    let completedGoals = goal.goalWorkouts.reduce(
        (accumulator, currentValue) => {
            if (currentValue.complete) return accumulator + 1;
            return accumulator;
        },
        0
    );

    return (
        <div className="main-div">
            <div className="back-button">
                <button onClick={loadDashboard}> Back to dashboard</button>
            </div>
            <div className="goal-header">
                <h1>Goal Details</h1>
                <i>
                    {goal.start_date} until {goal.end_date}
                </i>
                <h2>Goal {goal.goal_id}</h2>
            </div>
            <div className="goal-details">
                <div className="circle-container">
                    <div className="circle">
                        <GoalComplete achieved={goal.achieved} />
                    </div>
                </div>
            </div>
            <div className="goal-workouts">
                <p>
                    Completed workouts: {completedGoals} out of{' '}
                    {goal.goalWorkouts.length}
                </p>
                <progress value={completedGoals} max={goal.goalWorkouts.length}>
                    {' '}
                    progress bar
                </progress>
                <div className="goal-info">
                    {goal.goalWorkouts.map((item, index) => (
                        <div className="workout-info" key={index}>
                            <DisplayTraining
                                item={item.workout}
                                name={'Workout'}
                                addGoalButton={false}
                            />
                            <GoalWorkoutComplete
                                workout={item}
                                goal_id={goal_id}
                                recalculateGoal={getGoal}
                            />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
}

export default GoalDetails;
