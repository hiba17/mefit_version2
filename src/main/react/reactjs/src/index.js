import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Auth0Provider} from "@auth0/auth0-react";

ReactDOM.render(
    <Auth0Provider
        domain='dev-mefit.eu.auth0.com'
        clientId='JVmLQlaOjuwyo5Nw32PK4YPUD9eFKqqc'
        redirectUri={window.location.origin}
        audience='me-fit-endpoint.com'
    >
        <React.StrictMode>
            <App/>
        </React.StrictMode>
    </Auth0Provider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
