import {apiAudience, apiLink, fetchFromDatabase} from "../sharedComponents/ApiService";
import {useAuth0} from "@auth0/auth0-react";
import React, {useEffect, useState} from "react";
import {ProfileDisplay} from "./ProfileDisplay";
import './AssignContributor.css'

function AssignContributor() {
    const {getAccessTokenSilently} = useAuth0();
    const [profiles, setProfiles] = useState();

    useEffect(() => {
        getRequestedContributors()
    }, []);

    const getRequestedContributors = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, apiLink + 'profile/all')
            .then(response => {
                setProfiles(response.filter(profile => profile.contributor))
            })
    }

    const setToContributor = async (profile_id) => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, apiLink + "profile/" + profile_id + '/set-to-contributor').then(getRequestedContributors)
    }

    if(profiles === undefined) {
        return (<div></div>)
    }

    if(profiles.length === 0) {
        return (<div className='assign-contributor'>
            <h2>No pending requests for contributor role</h2>
        </div>)
    }

    return(<div className='assign-contributor'>
        <div>        <h2>The following users have requested to become a contributor:</h2>

        {profiles.map((profile, index) => (
            <div key={index} className='requested-contributors-display'>
                <ProfileDisplay profile = {profile} setToContributor={setToContributor}/>
            </div>
        ))}
        </div>
    </div>)
}

export default AssignContributor
