export function ProfileDisplay(props) {

    const onButtonPress = () => {
        props.setToContributor(props.profile.profile_id)
    }

    return (
        <div className='requested-contributors'>
            <div className='user-info'>
                <p>Email: {props.profile.email}</p>
                <p>First name: {props.profile.first_name}</p>
                <p>Last name: {props.profile.last_name}</p>
            </div>
            <div className='add-button'>
                <button onClick={onButtonPress}>Add as contributor</button>
            </div>
        </div>
    )
}
