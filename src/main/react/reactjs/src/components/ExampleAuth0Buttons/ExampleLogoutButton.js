import React from "react";
import { useAuth0 } from "@auth0/auth0-react";
import { useEffect } from "react";
import {LogOutButton} from "../../navbar/NavbarElement";

const ExampleLogoutButton = () => {
    // Gets the logout function from Auth0, which will be used to log out.
    const { logout } = useAuth0();

    const clearLocalStorage = () => {
        localStorage.clear();
    }

    return (
        <LogOutButton onClick={() => {
            logout({ returnTo: window.location.origin });
            clearLocalStorage();
        } }>
            Log Out
        </LogOutButton>
    );
};

export default ExampleLogoutButton;
