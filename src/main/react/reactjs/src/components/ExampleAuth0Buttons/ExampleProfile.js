import React from "react";
import {useAuth0} from "@auth0/auth0-react";
import {fetchFromDatabase} from "../../sharedComponents/ApiService";


const apiAudience = 'me-fit-endpoint.com';

function ExampleProfile() {
    // user: Holds information about the logged in user
    // isAuthenticated: Boolean to check if user is authenticated
    // getAccessTokenSilently: Gets JWL token from the Auth0 server, if necessary
    const {user, isAuthenticated, getAccessTokenSilently} = useAuth0();
    const getWorkout = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/workout/1')
    }

    const getExercisesInWorkout = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/workout/1/exercises')
    }

    const deleteExercise = async () => {
        await fetchFromDatabase({
            method: 'DELETE',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/exercise/1')
    }

    const getProfileFromEmail = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                Email: user.email
            }
        }, 'http://localhost:8080/api/v1/exercise/1')
    }

    const getProfile = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/profile/1')
    }

    const getGoalWithDates = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                end_date: "2021-03-28",
                start_date: "2021-03-22"
            }
        }, 'http://localhost:8080/api/v1/profile/1/goals/between-dates')
    }

    const deleteWorkout = async () => {
        await fetchFromDatabase({
            method: 'DELETE',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/workout/1')
    }

    const updateExercise = async () => {
        await fetchFromDatabase({
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                exercise_id: 1,
                description: 'New description',
                image_link: 'Link to new image',
                name: 'new name',
                target_muscle_group: 'Bones',
                vid_link: 'Link to video'
            })
        }, 'http://localhost:8080/api/v1/exercise')
    }

    const getAllGoalsFromProfile = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/profile/1/goals')
    }

    const deleteProfile = async () => {
        await fetchFromDatabase({
            method: 'DELETE',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/profile/1')
    }

    const getProgram = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/program/2')

    }
    const getAllPrograms = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/program')

    }

    const createProgram = async () => {
        await fetchFromDatabase({
            method: 'POST',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                category: 'training',
                name: 'Getting better',
                workoutList: [{workout_id: 1}, {workout_id: 2}],
                contributor_profile: {profile_id: 1}
            })
        }, 'http://localhost:8080/api/v1/program')
    }

    const deleteProgram = async () => {
        await fetchFromDatabase({
            method: 'DELETE',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/program/2')
    }

    const updateProgram = async () => {
        await fetchFromDatabase({
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json',
                body: JSON.stringify({
                    category: 'training',
                    name: 'Getting better',
                    workoutList: [{workout_id: 1}]
                })
            }
        }, 'http://localhost:8080/api/v1/program/2')
    }

    const getGoal = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/goal/1')
    }

    const deleteGoal = async () => {
        await fetchFromDatabase({
            method: 'DELETE',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/goal/2')
    }

    const createGoal = async () => {
        await fetchFromDatabase({
            method: 'POST',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                achieved: false,
                end_date: "2021-03-28",
                start_date: "2021-03-22",
                profile: {profile_id: 4},
                goalWorkouts: [
                    {workout: {workout_id: 1}, complete: false},
                    {workout: {workout_id: 2}, complete: true}]
            })
        }, 'http://localhost:8080/api/v1/goal')
    }

    const addWorkoutToGoal = async () => {
        await fetchFromDatabase({
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                workout: {
                    workout_id: 2
                },
                complete: true
            })
        }, 'http://localhost:8080/api/v1/goal/3/add-workout')
    }

    const removeWorkoutFromGoal = async () => {
        await fetchFromDatabase({
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                goalWorkout_id: 1
            })
        }, 'http://localhost:8080/api/v1/goal/1/remove-workout')
    }

    const updateGoal = async () => {
        await fetchFromDatabase({
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                goal_id: 1,
                achieved: true,
                end_date: "2021-03-28",
                start_date: "2021-03-22"
            })
        }, 'http://localhost:8080/api/v1/goal')
    }

    const updateProfile = async () => {
        await fetchFromDatabase({
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                profile_id: 1,
                email: "blahblah@blah.com",
                first_name: "first",
                last_name: "last",
                fitness_evaluation: "low",
                height: 180,
                weight: 200
            })
        }, 'http://localhost:8080/api/v1/profile')
    }

    const getTesting = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, 'http://localhost:8080/api/v1/profile/1/set-to-contributor')
    }

    return (
        isAuthenticated && (
            <div>
                <img src={user.picture} alt={user.name}/>
                <h2>{user.name}</h2>
                <p>{user.email}</p>
                <h3>User role</h3>
                <p></p>
                <p>{user["https://me-fit-endpoint.com/roles"]}</p>
                <button onClick={getExercisesInWorkout}>getExercisesInWorkout</button>
                <button onClick={getWorkout}>getWorkout</button>
                <button onClick={getProfileFromEmail}>getProfileFromEmail</button>
                <button onClick={deleteExercise}>deleteExercise</button>
                <button onClick={getGoalWithDates}>getGoalWithDates</button>
                <button onClick={deleteWorkout}>deleteWorkout</button>
                <button onClick={updateExercise}>updateExercise</button>
                <button onClick={getProfile}>getProfile</button>
                <button onClick={deleteProfile}>deleteProfile</button>
                <button onClick={getAllGoalsFromProfile}>getAllGoalsFromProfile</button>
                <button onClick={getProgram}>getProgram</button>
                <button onClick={getAllPrograms}>getAllPrograms</button>
                <button onClick={createProgram}>createProgram</button>
                <button onClick={deleteProgram}>deleteProgram</button>
                <button onClick={updateProgram}>updateProgram</button>
                <button onClick={createGoal}>createGoal</button>
                <button onClick={deleteGoal}>deleteGoal</button>
                <button onClick={getGoal}>getGoal</button>
                <button onClick={addWorkoutToGoal}>addWorkoutToGoal</button>
                <button onClick={updateGoal}>updateGoal</button>
                <button onClick={removeWorkoutFromGoal}>removeWorkoutFromGoal</button>
                <button onClick={updateProfile}>updateProfile</button>
                <button onClick={getTesting}>getTesting</button>
            </div>
        )
    );
}
;

export default ExampleProfile;
