import React from "react"
import { useAuth0 } from "@auth0/auth0-react";

const ExampleLoginButton = () => {
    // Gets the loginWithRedirect from Auth0.
    const { loginWithRedirect } = useAuth0();

    return <button onClick={() => loginWithRedirect()}>Login/Sign up</button>;
};

export default ExampleLoginButton

