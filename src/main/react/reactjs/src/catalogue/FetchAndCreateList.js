import React, { useState, useEffect } from 'react';
import { useAuth0 } from '@auth0/auth0-react';
import './FetchAndCreateList.css';
import DisplayTraining from './displayComponents/DisplayTraining';
import DisplayDropDownMenu from './displayComponents/DisplayDropDownMenu';
import { apiLink, fetchFromDatabase } from '../sharedComponents/ApiService';

function FetchAndCreateList(props) {
    const [training, setTraining] = useState([]);
    const [profile, setProfile] = useState({});
    const [filter, setFilter] = useState([]);
    const { getAccessTokenSilently } = useAuth0();
    const currentId = localStorage.getItem('currentId');
    const apiAudience = 'me-fit-endpoint.com';

    useEffect(() => {
        fetchFitnessEvaluation();
        fetchNameImg();
    }, [props.apiurl]);
    useEffect(() => {
        setFilter(training);
    }, [training]);

    const fetchFitnessEvaluation = async () => {
        fetchFromDatabase(
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${await getAccessTokenSilently({
                        audience: apiAudience,
                    })}`,
                },
            },
            apiLink + 'profile/' + currentId
        )
            .then((data) => setProfile(data))
            .catch((error) => console.log(error));
    };

    const fetchNameImg = async () => {
        fetchFromDatabase(
            {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${await getAccessTokenSilently({
                        audience: apiAudience,
                    })}`,
                },
            },
            props.apiurl
        )
            .then((data) => setTraining(data))
            .catch((error) => console.log(error));
    };

    const recommend = () => {
        let recommended = [];
        if (Object.keys(profile).length !== 0 && training.length !== 0) {
            if (props.name === 'Program' || props.name === 'Workout') {
                //Fitness_level:low== difficulty.level on activity:1
                if (profile.fitness_evaluation === 'low') {
                    for (let i = 0; i < training.length; i++) {
                        if (training[i].difficulty_level === 'low') {
                            recommended.push(training[i]);
                        }
                    }
                } else if (profile.fitness_evaluation === 'medium') {
                    //Fitness_level:medium== difficulty.level on activity:2
                    for (let i = 0; i < training.length; i++) {
                        if (training[i].difficulty_level === 'medium') {
                            recommended.push(training[i]);
                        }
                    }
                } else {
                    //Fitness_level:high== difficulty.level on activity:3
                    for (let i = 0; i < training.length; i++) {
                        if (training[i].difficulty_level === 'high') {
                            recommended.push(training[i]);
                        }
                    }
                }
            }
        }
        return recommended;
    };

    const title = () => {
        if (props.name === 'Program' || props.name === 'Workout') {
            return 'Recommended ' + props.name + 's ';
        } else {
            return '';
        }
    };

    const getDrowDownItems = () => {
        let list = [];
        if (props.name === 'Program') {
            for (let i = 0; i < training.length; i++) {
                list.push(training[i].category);
            }
        } else if (props.name === 'Workout') {
            for (let i = 0; i < training.length; i++) {
                list.push(training[i].type);
            }
        } else {
            for (let i = 0; i < training.length; i++) {
                list.push(training[i].target_muscle_group);
            }
        }
        list.push('All');
        return list.filter((item, i, ar) => ar.indexOf(item) === i);
    };

    const setItem = (filterItem) => {
        let listWithFilters = [];
        if (filterItem !== 'All') {
            if (props.name === 'Program') {
                for (let i = 0; i < training.length; i++) {
                    if (training[i].category == filterItem) {
                        listWithFilters.push(training[i]);
                    }
                }
            } else if (props.name === 'Workout') {
                for (let i = 0; i < training.length; i++) {
                    if (training[i].type == filterItem) {
                        listWithFilters.push(training[i]);
                    }
                }
            } else {
                for (let i = 0; i < training.length; i++) {
                    if (training[i].target_muscle_group == filterItem) {
                        listWithFilters.push(training[i]);
                    }
                }
            }
        } else {
            listWithFilters = training;
        }
        setFilter(listWithFilters);
    };

    return (
        <div>
            <div>
                <h2 className="lists"> {title()}</h2>
                <div className="recommended-list">
                    {recommend().map((item, index) => (
                        <div
                            key={index}
                            className="suggestion"
                            style={{
                                backgroundImage: `url(${item.image_link})`,
                            }}
                        >
                            <DisplayTraining
                                item={item}
                                name={props.name}
                                addGoalButton={true}
                            />
                        </div>
                    ))}
                </div>
            </div>
            <div className="allList">
                <h2 className="lists">All {props.name}s</h2>
                <div className="dropdown">
                    <button className="dropbtn"> Filter ᐯ</button>
                    <div className="dropdown-content">
                        {getDrowDownItems().map((item, index) => (
                            <div key={index}>
                                <DisplayDropDownMenu
                                    item={item}
                                    setItem={setItem}
                                />
                            </div>
                        ))}
                    </div>
                </div>
                {filter.map((item, index) => (
                    <div className="allProgramsList" key={index}>
                        <DisplayTraining
                            item={item}
                            name={props.name}
                            addGoalButton={true}
                        />
                    </div>
                ))}
            </div>
        </div>
    );
}

export default FetchAndCreateList;
