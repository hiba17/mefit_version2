import React from 'react'


const DisplayGoalId = props => {

    return (
        <div>
            <span onClick={() => props.setGoalId(props.goal.goal_id)}>
                <button>{props.goal.name}</button>
            </span>
        </div>
    );
};

export default DisplayGoalId
