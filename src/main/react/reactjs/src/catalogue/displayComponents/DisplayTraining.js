import React, {useEffect, useState} from 'react'
import Popup from "../Popup";
import WorkoutDetails from "../trainingDetails/WorkoutDetails";
import ExerciseDetails from "../trainingDetails/ExerciseDetails";
import ProgramDetails from "../trainingDetails/ProgramDetails";
import './DisplayTraining.css'
import {useAuth0} from "@auth0/auth0-react";
import {apiLink, fetchFromDatabase} from "../../sharedComponents/ApiService";
import DisplayGoalId from "./DisplayGoalId";

function DisplayTraining(props) {

    const apiAudience = 'me-fit-endpoint.com';
    const {getAccessTokenSilently} = useAuth0();
    const [isOpenDetails, setIsOpenDetails] = useState(false);
    const [allGoals, setAllGoals] = useState([])
    const [isOpenMessageProgram, setIsOpenMessageProgram] = useState(false);
    const [isOpenMessageWorkout, setIsOpenMessageWorkout] = useState(false);
    const [isOpenGoalIds, setIsOpenGoalIds] = useState(false);
    const currentId = localStorage.getItem('currentId');
    const endDate = localStorage.getItem('EndDate');
    const startDate = localStorage.getItem('StartDate');
    const [isGoalChosen, setIsGoalChosen] = useState(false)

    useEffect(() => {
        getAllGoalsFromProfile()

    }, [])

    const togglePopupDetails = () => {
        setIsOpenDetails(!isOpenDetails);
    }
    const togglePopupMessageProgram = () => {
        setIsOpenMessageProgram(!isOpenMessageProgram);
    }
    const togglePopupMessageWorkout = () => {
        setIsOpenMessageWorkout(!isOpenMessageWorkout);
    }
    const togglePopupGoalIDs = () => {
        console.log("toggle");
        setIsOpenGoalIds(!isOpenGoalIds);
    }
    const details = (item) => {

        if (props.name === "Program") {
            return (<Popup trene={<> <ProgramDetails props={item}/></>} handleClose={togglePopupDetails}/>)
        } else if (props.name === "Workout") {
            return (<Popup trene={<> <WorkoutDetails props={item}/></>} handleClose={togglePopupDetails}/>)
        } else {
            return (<Popup trene={<> <ExerciseDetails props={item}/></>} handleClose={togglePopupDetails}/>)
        }
    }

    const addWorkoutToGoal = async (id) => {
        await fetchFromDatabase({
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                workout: {
                    workout_id: props.item.workout_id
                },
                complete: false
            })
        }, `${apiLink}goal/${id}/add-workout`)

    }
    const createGoal = async (goalworkouts, name) => {
        await fetchFromDatabase({
            method: 'POST',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                achieved: false,
                end_date: endDate,
                start_date: startDate,
                profile: {profile_id: currentId},
                goalWorkouts: goalworkouts,
                name: name
            })
        }, apiLink + 'goal')

    }
    const setGoalWorkouts = () => {
        let goalWorkoutsList = []
        console.log(props.item.workoutList)
        props.item.workoutList.map(workout =>(
            goalWorkoutsList.push({workout: {workout_id: workout.workout_id },complete:false,name: workout.name})
        ))
        return goalWorkoutsList
    }
    const insertGoalProgram = (name) =>{
        createGoal(setGoalWorkouts(),name)
    }
    const getAllGoalsFromProfile = async () => {
        await fetchFromDatabase({
            method: 'GET',
            headers: {Authorization: `Bearer ${await getAccessTokenSilently({audience: apiAudience})}`}
        }, `${apiLink}profile/${currentId}/goals`).then(data => setAllGoals(data))
        console.log(props.name)
    }
    const setGoalId = (id) => {
        addWorkoutToGoal(id)
        setIsGoalChosen(!isGoalChosen)
    }
    const popupGoalIds = () => {
        return (
            <Popup trene={<div className="popupGoals">
                <div className="popupGoalTitle">
                {!isGoalChosen &&
                <h4>Which goal would you like to add the workout to?</h4>}
                </div>
                <div className="popupGoalList">
                {!isGoalChosen &&
                allGoals.map((goal, index) => (
                    <div className="goalPickerList" key={index}>
                        <DisplayGoalId goal={goal} setGoalId={setGoalId}/>

                    </div>
                ))}
                </div>
                {isGoalChosen &&
                <h4> The workout {props.item.name} has been added to your goal {props.goal_id} :)</h4>
                }

            </div>} handleClose={togglePopupGoalIDs}/>
        )
    }
    return (
        <div className="inner">
             <span className="card" onClick={() => togglePopupDetails()}>
                <div>
                    {props.name==="Exercise" ?(
                        <p className="item-name-exercise">{props.item.name} <br/>
                         </p>
                        ):
                        (<p className="item-name">{props.item.name}</p> )
                    }
                    {isOpenDetails && details(props.item)}

                </div>
             </span>
            {props.addGoalButton && (props.name === "Program") && <button className="catalogue-button" onClick={ () =>{insertGoalProgram(props.item.name);togglePopupMessageProgram()} }> Add as Goal</button>}
            {isOpenMessageProgram && <Popup trene={ <> <h4> The program {props.item.name}  has been added to you goals list :)</h4>
            </>} handleClose={togglePopupMessageProgram}/> }

            {props.addGoalButton && (props.name === "Workout") &&
            <div>
                <button className="catalogue-button-list"  onClick={()=>togglePopupGoalIDs()}>Add to a Goal</button>
                {isOpenGoalIds && popupGoalIds()}
            </div>
            }


        </div>
    )
}

export default DisplayTraining

