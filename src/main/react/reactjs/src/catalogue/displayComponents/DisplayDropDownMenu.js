import React from 'react'
import './DisplayDropDownMenu.css'

const DisplayDropDownMenu = props => {

    return (
        <span className="drop-down-menu" onClick={() => props.setItem(props.item)}>
                     <button className="drop-down-buttons">{props.item}</button>
                </span>
    );
};

export default DisplayDropDownMenu
