

//filtrer baser på: 1) program/workout/exercise og type/categori / musclegruppe
import React from 'react'
import './FilterNavbar.css'

function FilterNavbar(props) {
    const sendDataWorkout= e => {
        props.data(1)
    };
    const sendDataExercise= e => {
        props.data(2)
    };
    const sendDataProgram= e => {
        props.data(3)
    };
    return (
        <div className="FilterNavbar">
        <ul className="navbar">
        <li className="navbar_item"><a onClick={sendDataProgram}>Programs</a></li>
        <li className="navbar_item"><a  onClick={sendDataWorkout}>Workouts</a></li>
        <li className="navbar_item"><a onClick={sendDataExercise}>Exercises</a></li>
      </ul>
        </div>
        
    )
}

export default FilterNavbar
