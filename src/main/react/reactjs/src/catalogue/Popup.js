import React from 'react'
import './Popup.css'

const Popup = props => {
    console.log("popupopo!")
  return (
    <div className="popup-box">
      <div className="box">

        <span className="close-icon" onClick={props.handleClose}>x</span>
        {props.trene}
      </div>
    </div>
  );
};

export default Popup
