import React, { useEffect,useState } from 'react'
import FilterNavbar from './FilterNavbar.js'
import FetchAndCreateList from "./FetchAndCreateList";
import './TrainingCatalogue.css'
import {apiLink} from "../sharedComponents/ApiService";

function TrainingCatalogue() {
    const currentDate = new Date();
    const dayOfWeek = currentDate.getDay();
    const calcMonday =
        currentDate.getDate() - dayOfWeek + (dayOfWeek === 0 ? -6 : 1);
    let startDate = new Date(currentDate.setDate(calcMonday));
    let endDate = new Date().setDate(startDate.getDate() + 6);

    let tzoffset = (new Date()).getTimezoneOffset() * 60000;

    endDate = new Date(endDate- tzoffset).toISOString().slice(0, 10);
    startDate = new Date(startDate - tzoffset).toISOString().slice(0, 10);


    localStorage.setItem('StartDate', startDate);
    localStorage.setItem('EndDate', endDate);



    const [title, setTitle] = useState("Program");
    const [data, setData] = useState();
    const apiUrl = apiLink + 'program';
    const [apiURL, setApiURL] = useState(apiUrl);
    useEffect(() => {
        handleData()
    },[data])


    const handleData= ()=>{
         if(data === 1){
            setDetails("Workout", apiLink + 'workout')
         }
         else if(data === 2){
            setDetails("Exercise", apiLink + 'exercise')
         }
         else{
            setDetails("Program", apiLink + 'program')
         }
         
     }
     const setDetails=(title, apiurl)=>{
       setTitle(title);
       setApiURL(apiurl)
     }
    const setDatas=(number)=>{
        setData(number)
    }

    return (
        <div className="training-catalogue" >
            <FilterNavbar data={setDatas}/>
            <div className="all_programs">
                 <FetchAndCreateList apiurl={apiURL} name={title}/>
            </div>
        </div>
    )
}

export default TrainingCatalogue


