import React from 'react'
import './ExerciseDetails.css'
export default function ExerciseDetails(props){
    return (
        <div>
            <h2> {props.props.name} </h2>
            <h3> Target Muscle Group: {props.props.target_muscle_group}</h3>
            <p className="description"> {props.props.description}</p>
            <img src={props.props.image_link} alt=""/>
            <iframe width="420" height="315"
                    src={props.props.vid_link}>
            </iframe>




        </div>

    )

}