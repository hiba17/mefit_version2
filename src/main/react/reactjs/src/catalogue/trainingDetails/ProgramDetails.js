import React from 'react'
import './ProgramAndWorkoutDetails.css'
export default function ProgramDetails(props){

    return (
        <div className="details">
            <h2> {props.props.name} </h2>
            <div className="first-box">
                <h4> Category :  {props.props.category} </h4>
                <h5> Difficulty level : {props.props.difficulty_level}</h5>
            </div>
            <h3> List of workouts you will do each day: </h3>
            {props.props.workoutList.map((workout,index) =>(
                <div key={index}>
                   <span className="list">{workout.name}</span>
                    <span className="list">Approx: {workout.time} min</span>
                </div>
            ))}

        </div>

    )

}
