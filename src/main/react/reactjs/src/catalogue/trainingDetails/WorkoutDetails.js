import React, {useState, useEffect} from 'react'
import {useAuth0} from "@auth0/auth0-react";
import {fetchFromDatabase, simpleApiLink} from "../../sharedComponents/ApiService";
import './ProgramAndWorkoutDetails.css'

export default function WorkoutDetails(props) {

    const {getAccessTokenSilently} = useAuth0();
    const [exercises, setExercises] = useState([]);
    const apiAudience = 'me-fit-endpoint.com';

    useEffect(() => {
        fetchExerciseSet()
    }, [])

    const fetchExerciseSet = async () => {
        fetchFromDatabase({
            method: 'GET',
            headers: {
                Authorization: `Bearer ${await getAccessTokenSilently({
                    audience: apiAudience
                })}`
            }
        }, simpleApiLink + props.props.exercises)
            .then(data => setExercises(data))
            .catch(error => console.log(error))
    }

    return (
        <div>
           <h2> {props.props.name} </h2>
           <h4> Type :  {props.props.type} </h4>
           <h5> Difficulty level : {props.props.difficulty_level}</h5>
            <h5> Approx: {props.props.time} min Per day </h5>
            <h3> List of exercises in this workout: </h3>
            {exercises.map((exercise, index) => (
                <div key={index}>
                   <span className="list">{ exercise.exercise.name} </span>
                    <span className="list"> repetitions: {exercise.repetitions}</span>
                </div>
            ))}

        </div>

    )

    /*MÅ legges til!

     */

}

