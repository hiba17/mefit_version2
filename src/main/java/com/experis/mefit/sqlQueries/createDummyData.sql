-- CREATE PROFILES
INSERT INTO profile (first_name, last_name, fitness_evaluation, height, weight, is_contributor, email)
VALUES ('Adelen', 'Neleda', 'low', 180, 80, false, 'admin@admin.com');
INSERT INTO profile (first_name, last_name, fitness_evaluation, height, weight, is_contributor, email)
VALUES ('Ursula', 'Alusru', 'medium', 180, 80, true, 'user@user.com');
INSERT INTO profile (first_name, last_name, fitness_evaluation, height, weight, is_contributor, email)
VALUES ('Connie', 'Einnoc', 'high', 180, 80, false, 'contributor@contributor.com');


-- CREATE EXERCISES
INSERT INTO exercise ( name, description, target_muscle_group, image_link, vid_link, contributor_id)
VALUES ('Situps', 'An exercise where you sit, and then go up.', 'abs', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS8DiPE8ArsT6U0j2UKp1KL2w6poI0Ym9v75g&usqp=CAU', 'https://www.youtube.com/embed/1fbU_MkV7NE', 1);
INSERT INTO exercise (name, description, target_muscle_group, image_link, vid_link, contributor_id)
VALUES ('Pushups', 'An exercise where you push, and then go up.', 'triceps', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT-NPuvcIKfZcj7qj8F_wvDpaJC8RJo_nINZQ&usqp=CAU', 'https://www.youtube.com/embed/_l3ySVKYVJ8', 1);
INSERT INTO exercise (name, description, target_muscle_group, image_link, vid_link, contributor_id)
VALUES ('Jumping jack', 'An exercise where you jump. Many times.', 'quadriceps', 'https://treningstips.org/bilder/jumping-jacks.jpg', 'https://www.youtube.com/embed/iSSAk4XCsRA', 2);
INSERT INTO exercise (name, description, target_muscle_group, image_link, vid_link, contributor_id)
VALUES ('Lunges', 'An exercise where you lunge, and then go up.', 'glutes', 'https://177d01fbswx3jjl1t20gdr8j-wpengine.netdna-ssl.com/wp-content/uploads/2018/02/24Life-cover-photo-lunge-annotated-D1.jpg', 'https://www.youtube.com/embed/7SMzPn4LGjQ', 2);
INSERT INTO exercise (name, description, target_muscle_group, image_link, vid_link, contributor_id)
VALUES ('Squats', 'An exercise where you squat down, and then go up.', 'glutes', 'https://www.runtastic.com/blog/wp-content/uploads/2018/07/how-to-squat-common-mistakes-1200x800-1.jpg', 'https://www.youtube.com/embed/U3HlEF_E9fo', 2);
INSERT INTO exercise (name, description, target_muscle_group, image_link, vid_link, contributor_id)
VALUES ('Dumbbell rows', 'An exercise where you lift dumbbells with a rowing motion.', 'biceps', 'https://i.ytimg.com/vi/roCP6wCXPqo/maxresdefault.jpg', 'https://www.youtube.com/embed/roCP6wCXPqo', 1);
INSERT INTO exercise (name, description, target_muscle_group, image_link, vid_link, contributor_id)
VALUES ('Single-leg deadlifts', 'Single-leg deadlifts require stability and leg strength. Grab a light to moderate dumbbell to complete this move.', 'back', 'https://media1.popsugar-assets.com/files/thumbor/j5gPVlYiDQr9IBsZyaspkXD7aLc/fit-in/2048xorig/filters:format_auto-!!-:strip_icc-!!-/2014/02/27/927/n/1922729/b9021e726b062c8f_kettlebell-photo/i/Single-Leg-Deadlift-Kettlebell.jpg', 'https://www.youtube.com/embed/b9bHy3ojQWA', 2);
INSERT INTO exercise (name, description, target_muscle_group, image_link, vid_link, contributor_id)
VALUES ('Exercycle', 'An exercise where you ride a stationary bike', 'legs', 'https://www.hola.com/imagenes/estar-bien/20190206136791/ventajas-inconvenientes-spinning-cs/0-643-792/proscontraspinning-t.jpg', 'https://www.youtube.com/embed/udhRFATkN44', 2);
INSERT INTO exercise (name, description, target_muscle_group, image_link, vid_link, contributor_id)
VALUES ('Running', 'An exercise where you run', 'core', 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/summer-running-1597413181.jpg?crop=1xw:0.75xh;center,top&resize=1200:*', 'https://www.youtube.com/embed/_kGESn8ArrU', 1);


-- CREATE WORKOUTS
INSERT INTO workout (name, type, time, contributor_profile_id, difficulty_level, image_link)
VALUES ('FAT OFF MUSCLES ON', 'heavy lifting', 30, 1,'low', 'https://www.exercise.co.uk/wp/wp-content/uploads/2019/04/Compound-Exercise-Workout.jpg?fbclid=IwAR06Ne6kKboUv55KSa2Gw4gg-J3GZZgmVqe6KHJy3YOsgODYhHx583Gc96c');
INSERT INTO workout (name, type, time, contributor_profile_id,difficulty_level, image_link)
VALUES ('Big Pumps', 'cardio', 10, 1,'medium', 'https://miro.medium.com/max/14144/1*toyr_4D7HNbvnynMj5XjXw.jpeg');
INSERT INTO workout (name, type, time, contributor_profile_id, difficulty_level, image_link)
VALUES ('Run Like a Tiger!', 'cardio', 45, 1,'high', 'https://www.genesishealth.com/app/files/public/21170/TrainingToRunInterior.jpg');
INSERT INTO workout (name, type, time, contributor_profile_id, difficulty_level, image_link)
VALUES ('Bootylicious', 'endurance', 45, 2,'high', 'https://www.sats.no/globalassets/traning/reasons/new-gx-images/se_bootylicious_700x394.jpg');
INSERT INTO workout (name, type, time, contributor_profile_id, difficulty_level, image_link)
VALUES ('Absolution', 'core workout', 30, 2,'medium', 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/fitness-woman-doing-abs-crunches-royalty-free-image-579128718-1551795417.jpg?crop=1.00xw:0.796xh;0,0.0102xh&resize=1200:*');
INSERT INTO workout (name, type, time, contributor_profile_id, difficulty_level, image_link)
VALUES ('Beginner Spinning', 'cardio', 30, 2,'low', 'https://images.interactives.dk/files/bonnier-atr/spinningstime_med_instruktoer.jpg?auto=compress&ch=Width%2CDPR&dpr=2.63&ixjsv=2.2.4&q=38&rect=27%2C0%2C1445%2C1000&w=400');
INSERT INTO workout (name, type, time, contributor_profile_id, difficulty_level, image_link)
VALUES ('Advanced Spinning', 'cardio', 60, 2,'high', 'https://blog.mapmyrun.com/wp-content/uploads/2018/11/4-Ways-Online-Spinning-Classes-Helped-Me-on-My-Bike--752x472.jpg');
INSERT INTO workout (name, type, time, contributor_profile_id, difficulty_level, image_link)
VALUES ('Pilates', 'core workout', 20, 1,'low', 'http://lifestylegolfmagazine.se/wp-content/uploads/pilates-1.jpg');
INSERT INTO workout (name, type, time, contributor_profile_id, difficulty_level, image_link)
VALUES ('Yoga', 'core workout', 40, 1,'medium', 'https://labrador-www.kk.no/images/70201315.jpg?imageId=70201315&panow=0&panoh=0&panox=0&panoy=0&heightw=0&heighth=0&heightx=0&heighty=0&width=1200&height=630');

-- CREATE PROGRAMS
INSERT INTO program (name, category,difficulty_level,image_link)
VALUES ('Bum Challenge', 'LEG-Day','medium','https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/practice-and-persistence-gets-you-to-your-peak-royalty-free-image-1595532874.jpg');
INSERT INTO program (name, category,difficulty_level,image_link)
VALUES ('Summer Shred Challenge', 'SHRED-Power','high','http://upl.stack.com/wp-content/uploads/2015/12/17010133/Stack-Lunges-outside.jpg');
INSERT INTO program (name, category,difficulty_level,image_link)
VALUES ('Full Body Work Challenge', 'Beginner friendly','low','https://post.healthline.com/wp-content/uploads/2020/02/man-exercising-plank-push-up-1200x628-facebook.jpg');
INSERT INTO program (name, category,difficulty_level,image_link)
VALUES ('Get in Shape', 'Beginner friendly','medium','https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/practice-and-persistence-gets-you-to-your-peak-royalty-free-image-1595532874.jpg');
INSERT INTO program (name, category,difficulty_level,image_link)
VALUES ('Mass GAINS', 'Muscle Pump','high','https://us.123rf.com/450wm/fotokvadrat/fotokvadrat1601/fotokvadrat160100008/50925816-muscle-athlete-man-in-gym-making-elevations-bodybuilder-training-in-gym.jpg?ver=6');
INSERT INTO program (name, category,difficulty_level,image_link)
VALUES ('Flex n stretch', 'Flexibility','low','https://www.moneycrashers.com/wp-content/uploads/2011/01/woman-doing-a-barre-workout-mirror-ballet-stretch-810x455.jpg');
INSERT INTO program (name, category,difficulty_level,image_link)
VALUES ('Oh My Quad', 'LEG-Day','medium','https://hips.hearstapps.com/rbk.h-cdn.co/assets/16/48/1480371756-leg-workout.jpg');
INSERT INTO program (name, category,difficulty_level,image_link)
VALUES ('Sweat is just FAT CRYING!', 'Muscle Pump','high','https://recxpress.com.au/wp-content/uploads/2019/05/The-Importance-of-Good-Weightlifting-Form-and-How-to-Improve-Your-Technique.jpg');
INSERT INTO program (name, category,difficulty_level,image_link)
VALUES ('Round is a Shape', 'Beginner friendly','low','https://media1.popsugar-assets.com/files/thumbor/7EaDsjK4_FQJWqgR4BXG6ZIHU9s/fit-in/728xorig/filters:format_auto-!!-:strip_icc-!!-/2018/12/28/846/n/1922729/02f8b6105c26771fc830f0.52460620_/i/Workout-Plan-Women.jpg');


--INSERT WORKOUTS INTO PROGRAMS
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (1,1);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (2,1);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (2,2);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (3,2);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (1,3);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (3,3);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (3,8);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (4,4);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (9,4);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (6,5);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (2,8);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (1,5);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (5,5);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (5,6);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (4,6);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (7,7);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (7,6);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (8,9);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (8,4);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (9,8);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (9,7);
INSERT INTO workout_in_program (workout_id, program_id)
VALUES (3,9);


-- CREATE EXERCISE SET
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (1, 20, 1);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (2, 10, 2);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (3, 5, 3);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (4, 3, 4);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (4, 15, 5);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (5, 12, 6);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (6, 6, 4);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (6, 20, 5);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (7, 4, 6);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (7, 5, 1);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (7, 8, 3);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (8, 8, 2);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (8, 2, 4);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (9, 5, 5);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (9, 12, 6);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (9, 10, 7);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (4, 20, 7);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (3, 6, 8);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (7, 8, 8);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (9, 6, 9);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (5, 10, 9);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (6, 12, 9);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (2, 9, 7);
INSERT INTO exercise_set (exercise_id, exercise_repetitions, workout_id)
VALUES (1, 12, 8);

-- CREATE GOALS
INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Bum Challenge', '2021-03-15', '2021-03-21', 1, true);
INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Summer Shred Challenge', '2021-03-22', '2021-03-28', 1, true);
INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Get in Shape', '2021-03-29', '2021-04-04', 1, true);
INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Full Body Work Challenge', '2021-03-29', '2021-04-04', 1, true);

INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Bum Challenge', '2021-03-22', '2021-03-28', 2, true);
INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Summer Shred Challenge', '2021-03-22', '2021-03-28', 2, true);
INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Get in Shape', '2021-03-29', '2021-04-04', 2, true);
INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Full Body Work Challenge', '2021-03-29', '2021-04-04', 2, true);

INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Bum Challenge', '2021-03-22', '2021-03-28', 3, true);
INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Summer Shred Challenge', '2021-03-22', '2021-03-28', 3, true);
INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Get in Shape', '2021-03-29', '2021-04-04', 3, true);
INSERT INTO goal (name, start_date, end_date, profile_id, achieved)
VALUES ('Full Body Work Challenge', '2021-03-29', '2021-04-04', 3, true);


-- ADD WORKOUTS TO PROGRAM
INSERT INTO workout_in_program (program_id, workout_id)
VALUES (1, 1);

-- ADD WORKOUT TO GOALS
INSERT INTO goal_workout (name, complete, goal_id, workout_id)
VALUES ('FAT OFF MUSCLES ON', true, 1, 1);
INSERT INTO goal_workout (name, complete, goal_id, workout_id)
VALUES ('Big pumps', true, 2, 2);
INSERT INTO goal_workout (name, complete, goal_id, workout_id)
VALUES ('Ruuuuuuun', true, 2, 3);
INSERT INTO goal_workout (name, complete, goal_id, workout_id)
VALUES ('Big pumps', true, 3, 2);
INSERT INTO goal_workout (name, complete, goal_id, workout_id)
VALUES ('Ruuuuuuun', true, 1, 3);
INSERT INTO goal_workout (name, complete, goal_id, workout_id)
VALUES ('FAT OFF MUSCLES ON', true, 4, 1);

