package com.experis.mefit.repositories;

import com.experis.mefit.models.GoalWorkout;
import com.experis.mefit.models.Workout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface GoalWorkoutRepository extends JpaRepository<GoalWorkout, Long> {
    @Transactional
    Long deleteByWorkout(Workout workout);
}
