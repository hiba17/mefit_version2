package com.experis.mefit.repositories;

import com.experis.mefit.models.Exercise;
import com.experis.mefit.models.ExerciseSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ExerciseSetRepository extends JpaRepository<ExerciseSet, Long> {
    @Transactional
    Long deleteByExercise(Exercise exercise);
}
