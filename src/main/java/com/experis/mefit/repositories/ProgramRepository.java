package com.experis.mefit.repositories;

import com.experis.mefit.models.Program;
import com.experis.mefit.models.Workout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProgramRepository extends JpaRepository<Program, Long> {
    List<Program> findByWorkoutListContaining(Workout workout);
}
