package com.experis.mefit.repositories;

import com.experis.mefit.models.Goal;
import com.experis.mefit.models.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface GoalRepository extends JpaRepository<Goal, Long> {
    @Transactional
    Long deleteByProfile(Profile profile);
}
