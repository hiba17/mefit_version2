package com.experis.mefit.repositories;

import com.experis.mefit.models.Exercise;
import com.experis.mefit.models.ExerciseSet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExerciseRepository extends JpaRepository<Exercise, Long> {
    Exercise findByExerciseSetListContaining(ExerciseSet exerciseSet);
}
