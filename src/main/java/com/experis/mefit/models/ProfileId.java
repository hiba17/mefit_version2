package com.experis.mefit.models;

public class ProfileId {
    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    private Long profileId;
}
