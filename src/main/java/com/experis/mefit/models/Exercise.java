package com.experis.mefit.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;

@Entity
public class Exercise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long exercise_id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String target_muscle_group;

    @Column
    private String image_link;

    @Column
    private String vid_link;

    @ManyToOne
    @JoinColumn(name = "contributor_id")
    private Profile contributor;

    @JsonGetter("contributor")
    public Long contributorGetter(){
        if(contributor != null)
            return contributor.getProfile_id();
        return null;
    }
    @OneToMany(mappedBy = "exercise")
    List<ExerciseSet> exerciseSetList;

    public long getExercise_id() {
        return exercise_id;
    }

    public void setExercise_id(long exercise_id) {
        this.exercise_id = exercise_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTarget_muscle_group() {
        return target_muscle_group;
    }

    public void setTarget_muscle_group(String target_muscle_group) {
        this.target_muscle_group = target_muscle_group;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public String getVid_link() {
        return vid_link;
    }

    public void setVid_link(String vid_link) {
        this.vid_link = vid_link;
    }

    public Profile getContributor() {
        return contributor;
    }

    public void setContributor(Profile contributor) {
        this.contributor = contributor;
    }
}
