package com.experis.mefit.models;

import javax.persistence.*;

@Entity
public class ExerciseSet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long exercise_set_id;

    @ManyToOne
    @JoinColumn(name = "exercise_id")
    private Exercise exercise;

    @Column
    private int exercise_repetitions;

    public long getExercise_set_id() {
        return exercise_set_id;
    }

    public void setExercise_set_id(long exercise_set_id) {
        this.exercise_set_id = exercise_set_id;
    }

    public int getExercise_repetitions() {
        return exercise_repetitions;
    }

    public void setExercise_repetitions(int exercise_repetitions) {
        this.exercise_repetitions = exercise_repetitions;
    }

    public Exercise getExercise() {
        return exercise;
    }
}
