package com.experis.mefit.models;


import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Profile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long profile_id;

    @Column(unique = true)
    private String email;

    @Column
    private String first_name;

    @Column
    private String last_name;

    @Column
    private double height;

    @Column
    private double weight;

    @Column
    private String fitness_evaluation;

    @Column
    private boolean isContributor;

    @OneToMany(mappedBy = "contributor")
    private List<Exercise> createdExercises;

    @OneToMany(mappedBy = "profile")
    List<Goal> userGoals;

    @JsonGetter("userGoals")
    public List<String> goalsGetter(){
        if(userGoals != null)
            return userGoals.stream().map(goal -> "api/v1/goal/" + goal.getGoal_id())
                    .collect(Collectors.toList());
        return null;
    }

    @OneToMany(mappedBy = "contributor_profile")
    private List<Program> program;

    public Profile() {
    }

    public long getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(long profile_id) {
        this.profile_id = profile_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getFitness_evaluation() {
        return fitness_evaluation;
    }

    public void setFitness_evaluation(String fitness_evaluation) {
        this.fitness_evaluation = fitness_evaluation;
    }

    public boolean isContributor() {
        return isContributor;
    }

    public void setContributor(boolean contributor) {
        isContributor = contributor;
    }

    public List<Goal> getUserGoals() {
        return userGoals;
    }

    public void setUserGoals(List<Goal> userGoals) {
        this.userGoals = userGoals;
    }
}
