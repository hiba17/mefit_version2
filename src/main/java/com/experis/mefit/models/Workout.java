package com.experis.mefit.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.util.List;

@Entity
public class Workout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long workout_id;

    @Column
    private String type;

    @Column
    private String name;
    @Column
    private int time;
    @Column
    private String difficulty_level;

    @Schema(example = "[{exercise_id: 0, exercise_repetitions: 0}]")
    @OneToMany
    @JoinColumn(name="workout_id")
    private List<ExerciseSet> exercises;

    @JsonGetter("exercises")
    public String exercisesGetters(){
        return "/api/v1/workout/" + workout_id + "/exercises";
    }

    @Column
    private long contributorProfile_id;

    @Column
    private String image_link;

    @ManyToMany(mappedBy = "workoutList")
    private List<Program> programList;

    public long getWorkout_id() {
        return workout_id;
    }

    public void setWorkout_id(long workout_id) {
        this.workout_id = workout_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ExerciseSet> getExercises() {
        return exercises;
    }

    public void setExercises(List<ExerciseSet> exercise_set_id) {
        this.exercises = exercise_set_id;
    }

    public long getContributorProfile_id() {
        return contributorProfile_id;
    }

    public void setContributorProfile_id(long contributorProfile_id) {
        this.contributorProfile_id = contributorProfile_id;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }

    public String getDifficulty_level() {
        return difficulty_level;
    }

    public void setDifficulty_level(String difficulty_level) {
        this.difficulty_level = difficulty_level;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
