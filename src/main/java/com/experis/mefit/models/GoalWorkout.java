package com.experis.mefit.models;

import javax.persistence.*;

@Entity
public class GoalWorkout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long goalWorkout_id;

    @ManyToOne
    @JoinColumn(name = "workout_id")
    private Workout workout;

    @Column
    private boolean complete;

    @Column
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getGoalWorkout_id() {
        return goalWorkout_id;
    }

    public void setGoalWorkout_id(long goalWorkout_id) {
        this.goalWorkout_id = goalWorkout_id;
    }

    public Workout getWorkout() {
        return workout;
    }

    public void setWorkout(Workout workout_id) {
        this.workout = workout_id;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }
}
