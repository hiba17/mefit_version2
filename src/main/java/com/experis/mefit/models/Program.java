package com.experis.mefit.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;

@Entity
public class Program {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long program_id;

    @Column
    private String name;

    @Column
    private String difficulty_level;

    @Column
    private String image_link;

    @Column
    private String category;

    public Profile getContributor_profile() {
        return contributor_profile;
    }

    public void setContributor_profile(Profile contributor_profile) {
        this.contributor_profile = contributor_profile;
    }

    @ManyToOne()
    @JoinColumn(name = "contributor_profile_id")
    private Profile contributor_profile;

    @JsonGetter("contributor_profile")
    public Long contributorGetter(){
        if(contributor_profile != null)
            return contributor_profile.getProfile_id();
        return null;
    }
    @ManyToMany
    @JoinTable(
            name = "workout_in_program",
            joinColumns = {@JoinColumn(name = "program_id")},
            inverseJoinColumns = {@JoinColumn(name = "workout_id")}
    )
    private List<Workout> workoutList;

    public long getProgram_id() {
        return program_id;
    }

    public void setProgram_id(long program_id) {
        this.program_id = program_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Workout> getWorkoutList() {
        return workoutList;
    }

    public void setWorkoutList(List<Workout> workoutList) {
        this.workoutList = workoutList;
    }

    public String getDifficulty_level() {
        return difficulty_level;
    }

    public void setDifficulty_level(String difficulty_level) {
        this.difficulty_level = difficulty_level;
    }

    public String getImage_link() {
        return image_link;
    }

    public void setImage_link(String image_link) {
        this.image_link = image_link;
    }
}
