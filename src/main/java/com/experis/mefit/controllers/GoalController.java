package com.experis.mefit.controllers;

import com.experis.mefit.models.Goal;
import com.experis.mefit.models.GoalWorkout;
import com.experis.mefit.models.Workout;
import com.experis.mefit.repositories.GoalRepository;
import com.experis.mefit.repositories.GoalWorkoutRepository;
import com.experis.mefit.repositories.ProfileRepository;
import com.experis.mefit.repositories.WorkoutRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/goal")
public class GoalController {

    @Autowired
    private GoalRepository goalRepository;
    @Autowired
    private GoalWorkoutRepository goalWorkoutRepository;
    @Autowired
    private WorkoutRepository workoutRepository;
    @Autowired
    private ProfileRepository profileRepository;

    @Operation(summary = "Get a goal by its id.")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('read:data')")
    @ApiResponses(value = {

            @ApiResponse(responseCode = "200", description = "Returned the exercise."),
            @ApiResponse(responseCode = "400", description = "Not a valid id",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Goal not found.",
                    content = @Content)})
    public ResponseEntity<Goal> getGoal(@PathVariable(value = "id") Long id) {
        if (id <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (!goalRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(goalRepository.findById(id).get(), HttpStatus.OK);
    }

    @Operation(summary = "Delete a goal by its id.")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('read:data')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Return OK."),
            @ApiResponse(responseCode = "400", description = "Not a valid id",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to delete content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Goal not found.",
                    content = @Content)})
    public ResponseEntity<HttpStatus> deleteGoal(@PathVariable(value = "id") Long id) {
        if (id <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (!goalRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        Goal goal = goalRepository.findById(id).get();
        for (GoalWorkout goalWorkout : goal.getGoalWorkouts()) {
            goalWorkoutRepository.delete(goalWorkout);
        }
        goalRepository.delete(goal);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Create a new goal.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created a new goal."),
            @ApiResponse(responseCode = "400", description = "The given profile or workout is null.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to create content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "The profile or a workout that is part of the goal does not exist.",
                    content = @Content)})
    @PostMapping
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<Goal> createGoal(@RequestBody Goal goal) {
        System.out.println(goal.getProfile());
        // Check that the profile is not null
        if (goal.getProfile() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        // Check if the profile id exists
        if (!profileRepository.existsById(goal.getProfile().getProfile_id())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        // Check that all the workouts exists and not null, and save the new GoalWorkouts
        for (GoalWorkout goalWorkout : goal.getGoalWorkouts()) {
            if (goalWorkout.getWorkout() == null) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            if (!workoutRepository.existsById(goalWorkout.getWorkout().getWorkout_id())) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            goalWorkoutRepository.save(goalWorkout);
        }
        updateGoalStatus(goal);
        return new ResponseEntity<>(goalRepository.save(goal), HttpStatus.OK);
    }

    @Operation(summary = "Add workout to goal.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Added workout to goal."),
            @ApiResponse(responseCode = "400", description = "The given workout is null.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to add workout to goal.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "The workout or goal does not exist.",
                    content = @Content)})
    @PutMapping(value = "/{id}/add-workout")
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<HttpStatus> addWorkoutToGoal(@PathVariable(value = "id") Long id,
                                                       @RequestBody GoalWorkout goalWorkout) {
        if (goalWorkout == null || goalWorkout.getWorkout() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Long workoutId = goalWorkout.getWorkout().getWorkout_id();
        if (!goalRepository.existsById(id) ||
                !workoutRepository.existsById(workoutId)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Goal goal = goalRepository.findById(id).get();
        List<GoalWorkout> goalWorkoutList = goal.getGoalWorkouts();
        // If the workout is already part of the goal, only update it's status.
        for(GoalWorkout existingGoalWorkout : goalWorkoutList) {
            if(workoutId == existingGoalWorkout.getWorkout().getWorkout_id()) {
                existingGoalWorkout.setComplete(goalWorkout.isComplete());
                goalWorkoutRepository.save(existingGoalWorkout);
                updateGoalStatus(goal);
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
        // Add a workout that exists in our database to our goal.
        goalWorkout = goalWorkoutRepository.save(goalWorkout);
        goalWorkoutList.add(goalWorkout);
        goalRepository.save(goal);
        updateGoalStatus(goal);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Remove workout from goal.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Added workout to goal."),
            @ApiResponse(responseCode = "401", description = "Not allowed to add workout to goal.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "The workout or goal does not exist.",
                    content = @Content)})
    @PutMapping(value = "/{id}/remove-workout")
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<HttpStatus> removeWorkoutFromGoal(@PathVariable(value = "id") Long id,
                                                            @RequestBody GoalWorkout goalWorkout) {
        Long goalWorkoutId = goalWorkout.getGoalWorkout_id();
        if (!goalWorkoutRepository.existsById(goalWorkoutId) ||
                !goalRepository.existsById(id)) {
            System.out.println(goalWorkoutId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Goal goalToBeUpdated = goalRepository.findById(id).get();
        List<GoalWorkout> goalWorkouts = goalToBeUpdated.getGoalWorkouts();
        GoalWorkout goalWorkoutToBeRemoved = null;
        for (GoalWorkout currentGoalWorkout : goalWorkouts) {
            if (currentGoalWorkout.getGoalWorkout_id() == goalWorkoutId) {
                goalWorkoutToBeRemoved = currentGoalWorkout;
                continue;
            }
        }
        if (goalWorkoutToBeRemoved == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        goalWorkouts.remove(goalWorkoutToBeRemoved);
        goalWorkoutRepository.delete(goalWorkout);
        goalRepository.save(goalToBeUpdated);
        updateGoalStatus(goalToBeUpdated);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Update goal.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated goal."),
            @ApiResponse(responseCode = "401", description = "Not allowed to update goal.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "The goal does not exist.",
                    content = @Content)})
    @PutMapping
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<HttpStatus> updateGoal(@RequestBody Goal goal) {
        if (!goalRepository.existsById(goal.getGoal_id())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Goal goalToBeUpdated = goalRepository.findById(goal.getGoal_id()).get();

        // If the achieved value has changed, update the workout achieved value as well.
        if (goal.isAchieved() != goalToBeUpdated.isAchieved()) {
            List<GoalWorkout> goalWorkouts = goalToBeUpdated.getGoalWorkouts();
            for(GoalWorkout goalWorkout : goalWorkouts) {
                goalWorkout.setComplete(goal.isAchieved());
                goalWorkoutRepository.save(goalWorkout);
            }
        }

        goalToBeUpdated.setAchieved(goal.isAchieved());
        goalToBeUpdated.setStart_date(goal.getStart_date());
        goalToBeUpdated.setEnd_date(goal.getEnd_date());
        goalRepository.save(goalToBeUpdated);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void updateGoalStatus(Goal goal){
        System.out.println("Updating goal status");
        List<GoalWorkout> goalWorkouts = goal.getGoalWorkouts();
        for(GoalWorkout goalWorkout : goalWorkouts) {
            if(!goalWorkout.isComplete()) {
                if(goal.isAchieved()) {
                    goal.setAchieved(false);
                    goalRepository.save(goal);
                }
                return;
            }
        }
        goal.setAchieved(true);
        goalRepository.save(goal);
    }
}

