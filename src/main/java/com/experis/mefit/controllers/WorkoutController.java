package com.experis.mefit.controllers;


import com.experis.mefit.models.*;
import com.experis.mefit.repositories.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/v1/workout")
public class WorkoutController {
    @Autowired
    private WorkoutRepository workoutRepository;
    @Autowired
    private ExerciseSetRepository exerciseSetRepository;
    @Autowired
    private ExerciseRepository exerciseRepository;
    @Autowired
    private GoalWorkoutRepository goalWorkoutRepository;
    @Autowired
    private ProgramRepository programRepository;

    /*
     * Returns info on workout with given id.
     */
    @Operation(summary = "Get info about the workout with given id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned the workout."),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Workout not found.",
                    content = @Content)})
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<Workout> getWorkout(@PathVariable(value = "id") long id) {
        if (id <= 0)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        if (!workoutRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(workoutRepository.findById(id).get(), HttpStatus.OK);
    }

    /*
     * Create a workout.
     */
    @Operation(summary = "Create a new workout.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created a new workout."),
            @ApiResponse(responseCode = "400", description = "Exercise included in workout does not exist.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to create content.",
                    content = @Content)})
    @PostMapping
    @PreAuthorize("hasAuthority('create:data')")
    public ResponseEntity<Workout> createWorkout(@RequestBody Workout workout) {
        for (ExerciseSet exerciseSet : workout.getExercises()) {
            exerciseSetRepository.save(exerciseSet);
        }
        return new ResponseEntity<>(workoutRepository.save(workout), HttpStatus.CREATED);
    }

    /*
     * Get all workouts.
     */
    @Operation(summary = "Get all the workouts in the database.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned the workouts."),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content)})
    @GetMapping()
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<List<Workout>> getAllWorkouts() {
        List<Workout> allWorkouts = workoutRepository.findAll();
        return new ResponseEntity<>(allWorkouts, HttpStatus.OK);
    }

    /*
     * Get all exercises in the workout, and the number of repetitions
     */
    @Operation(summary = "Get all the exercises part of a workout with given id, and the number of repetitions.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned the exercises and the number of repetitions."),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Workout not found.",
                    content = @Content)})
    @GetMapping("/{id}/exercises")
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<List<ExerciseRepetitions>> getAllExercisesInWorkout(@PathVariable(value = "id") long id) {
        if (id <= 0)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        if (!workoutRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        // Get the workout
        Workout workout = workoutRepository.findById(id).get();
        // Get the list of exercise sets in the workout
        List<ExerciseSet> exerciseSetList = workout.getExercises();
        // Create list to hold the exercises and the number of repetitions
        List<ExerciseRepetitions> exerciseRepetitionsList = new ArrayList<>();
        for (ExerciseSet exerciseSet : exerciseSetList) {
            ExerciseRepetitions exerciseRepetitions = new ExerciseRepetitions();
            exerciseRepetitions.setRepetitions(exerciseSet.getExercise_repetitions());
            Exercise exercise = exerciseRepository.findByExerciseSetListContaining(exerciseSet);
            exerciseRepetitions.setExercise(exercise);
            exerciseRepetitionsList.add(exerciseRepetitions);
        }
        return new ResponseEntity<>(exerciseRepetitionsList, HttpStatus.OK);
    }

    @Operation(summary = "Delete the workout with the given id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned the exercises and the number of repetitions."),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to delete content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Workout not found.",
                    content = @Content)})
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('create:data')")
    public ResponseEntity<HttpStatus> deleteWorkout(@PathVariable(value = "id") Long id) {
        if (id <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (!workoutRepository.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Workout workout = workoutRepository.findById(id).get();
        // Delete workout from goals
        goalWorkoutRepository.deleteByWorkout(workout);
        // Delete exercises from workout
        for(ExerciseSet exerciseSet : workout.getExercises()) {
            exerciseSetRepository.delete(exerciseSet);
        }
        // Delete workout from program
        List<Program> programList = programRepository.findByWorkoutListContaining(workout);
        for(Program program : programList) {
            program.getWorkoutList().remove(workout);
            programRepository.save(program);
        }
        // Delete workout
        workoutRepository.delete(workout);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /*
     * Update a workout.
     */
    @Operation(summary = "Update an existing workout.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the workout."),
            @ApiResponse(responseCode = "400", description = "Exercise included in the workout does not exist.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to create content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Workout with given id does not exist.",
                    content = @Content)})
    @PutMapping
    @PreAuthorize("hasAuthority('create:data')")
    public ResponseEntity<Workout> updateWorkout(@RequestBody Workout workout) {
        if (!workoutRepository.existsById(workout.getWorkout_id())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        // Remove the old exercises in the workout.
        Workout oldWorkout = workoutRepository.findById(workout.getWorkout_id()).get();
        for(ExerciseSet exerciseSet : oldWorkout.getExercises()) {
            exerciseSetRepository.delete(exerciseSet);
        }
        // Add the new exercises to the workout
        for (ExerciseSet exerciseSet : workout.getExercises()) {
            exerciseSetRepository.save(exerciseSet);
        }
        return new ResponseEntity<>(workoutRepository.save(workout), HttpStatus.CREATED);
    }
}
