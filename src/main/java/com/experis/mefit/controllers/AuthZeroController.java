package com.experis.mefit.controllers;

import com.experis.mefit.models.AccessToken;
import com.experis.mefit.models.Profile;
import com.experis.mefit.models.Roles;
import com.experis.mefit.repositories.ProfileRepository;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@RestController
@RequestMapping("api/v1/")
public class AuthZeroController {

    private String authToken = getAccessToken();

    private String getAccessToken(){
        AccessToken accessToken = new AccessToken();
        accessToken.setAudience("https://dev-mefit.eu.auth0.com/api/v2/");
        accessToken.setClient_id(System.getenv("clientid"));
        accessToken.setClient_secret(System.getenv("clientsecret"));
        accessToken.setGrant_type("client_credentials");

        if(System.getenv("clientsecret") == null || System.getenv("clientid") == null) {
            System.out.println("No environment variables set!");
        }

        String response = (String) Unirest.post("https://dev-mefit.eu.auth0.com/oauth/token")
                .contentType("application/json")
                .body(accessToken)
                .asJson()
                .getBody()
                .getArray()
                .getJSONObject(0)
                .get("access_token");
        return response;
    }

    @Autowired
    ProfileRepository profileRepository;

    @GetMapping(value = "profile/{id}/set-to-contributor")
    @PreAuthorize("hasAuthority('create:data')")
    public ResponseEntity<String> setToContributor(@PathVariable(value = "id") Long id) {
        if (id <= 0)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        if (!profileRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Profile profile = profileRepository.findById(id).get();
        HttpResponse<String> response = addRoleToUser("contributor", profile.getEmail());
        profile.setContributor(false);
        profileRepository.save(profile);


        return getStringResponseEntity(id, response);
    }

    @GetMapping(value = "profile/{id}/set-to-user")
    public ResponseEntity<String> setToUser(@PathVariable(value = "id") Long id) {
        if (id <= 0)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        if (!profileRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        Profile profile = profileRepository.findById(id).get();
        HttpResponse<String> response = addRoleToUser("user", profile.getEmail());

        return getStringResponseEntity(id, response);
    }

    private ResponseEntity<String> getStringResponseEntity(@PathVariable("id") Long id, HttpResponse<String> response) {
        if(response == null) {
            return  new ResponseEntity<>("UnsupportedEncodingException when trying to encode userId.", HttpStatus.INTERNAL_SERVER_ERROR);
        } else if(response.getStatus() == 204) {
            return new ResponseEntity<>("Added role to user with id " + id + ".", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Something went wrong when attempting to add a role to the user:" + response.getStatus(), HttpStatus.BAD_REQUEST);
        }
    }

    private HttpResponse<String> addRoleToUser(String roleName, String email) {
        // The userId auth0 has connected with the user
        String userId = getUserId(email);
        // All roles in auth0
        JSONArray allRoles = getAllRoleIds();
        // Role id of roleName
        String roleId = getSpecificRoleIdFromListOfRoles(allRoles, roleName);

        try {
            String encodedUserId = URLEncoder.encode(userId, "UTF-8");
            Roles userRoles = new Roles();
            userRoles.setRoles(new String[]{roleId});
            HttpResponse<String> response =  Unirest.post("https://dev-mefit.eu.auth0.com/api/v2/users/" + encodedUserId + "/roles")
                    .contentType("application/json")
                    .header("authorization", "Bearer " + authToken)
                    .body(userRoles)
                    .asString();
            return response;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getSpecificRoleIdFromListOfRoles(JSONArray allRoles, String role) {
        for(int i = 0; i < allRoles.length(); i++) {
            JSONObject roleObject = allRoles.getJSONObject(i);
            if(roleObject.get("name").equals(role)) {
                return roleObject.get("id").toString();
            }
        }
        return null;
    }

    private String getUserId(String email){
        return (String) Unirest.get("https://dev-mefit.eu.auth0.com/api/v2/users-by-email?fields=user_id&email=" + email)
                .header("authorization", "Bearer " + authToken)
                .asJson()
                .getBody()
                .getArray()
                .getJSONObject(0)
                .get("user_id");
    }

    private JSONArray getAllRoleIds () {
        return Unirest.get("https://dev-mefit.eu.auth0.com/api/v2/roles")
                .header("authorization", "Bearer " + authToken)
                .asJson()
                .getBody()
                .getArray();
    }
}
