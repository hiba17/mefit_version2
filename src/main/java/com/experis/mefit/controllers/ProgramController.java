package com.experis.mefit.controllers;

import com.experis.mefit.models.Program;
import com.experis.mefit.models.Workout;
import com.experis.mefit.repositories.ProgramRepository;
import com.experis.mefit.repositories.WorkoutRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/program")
public class ProgramController {

    @Autowired
    ProgramRepository programRepository;
    @Autowired
    WorkoutRepository workoutRepository;

    @Operation(summary = "Get a program by its id.")
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('read:data')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned the program."),
            @ApiResponse(responseCode = "400", description = "Not a valid id",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Program not found.",
                    content = @Content)})
    public ResponseEntity<Program> getProgram(@PathVariable(value = "id") Long id) {
        if (id <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (!programRepository.existsById(id)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(programRepository.findById(id).get(), HttpStatus.OK);
    }

    @Operation(summary = "Create a new program.")
    @PostMapping()
    @PreAuthorize("hasAuthority('create:data')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created the program."),
            @ApiResponse(responseCode = "401", description = "Not allowed to create content."),
            @ApiResponse(responseCode = "404", description = "Could not find content.")
    })
    public ResponseEntity<Program> createProgram(@RequestBody Program program) {
        for (Workout workout : program.getWorkoutList()) {
            if (!workoutRepository.existsById(workout.getWorkout_id())) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        }

        return new ResponseEntity<>(programRepository.save(program), HttpStatus.OK);
    }

    @Operation(summary = "Delete an existing program.")
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('create:data')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted the program."),
            @ApiResponse(responseCode = "401", description = "Not allowed to delete content."),
            @ApiResponse(responseCode = "404", description = "Could not find content.")
    })
    public ResponseEntity<HttpStatus> deleteProgram(@PathVariable(value = "id") Long id) {
        if (!programRepository.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        programRepository.delete(programRepository.findById(id).get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
    /*
     * Get all programs.
     */
    @Operation(summary = "Get all the programs in the database.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned the programs."),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content)})
    @GetMapping()
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<List<Program>> getAllPrograms() {
        List<Program> allPrograms = programRepository.findAll();
        return new ResponseEntity<>(allPrograms, HttpStatus.OK);
    }

    @Operation(summary = "Update an existing program.")
    @PutMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('create:data')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the program."),
            @ApiResponse(responseCode = "401", description = "Not allowed to modify content."),
            @ApiResponse(responseCode = "404", description = "Could not find content.")
    })
    public ResponseEntity<Program> updateProgram(@PathVariable(value = "id") Long id, @RequestBody Program program) {
        if(!programRepository.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        program.setProgram_id(id);
        return new ResponseEntity<>(programRepository.save(program), HttpStatus.OK);
    }

  
}
