package com.experis.mefit.controllers;

import com.experis.mefit.models.Goal;
import com.experis.mefit.models.Profile;
import com.experis.mefit.models.ProfileId;
import com.experis.mefit.models.Roles;
import com.experis.mefit.repositories.GoalRepository;
import com.experis.mefit.repositories.ProfileRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/profile")
public class ProfileController {

    @Autowired
    ProfileRepository profileRepository;

    @Autowired
    GoalRepository goalRepository;

    @Autowired
    GoalController goalController;

    @Operation(summary = "Returns a list of profiles where is_contributor is true.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned a list of profiles."),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content)})
    @GetMapping(value = "/all")
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<List<Profile>> getProfilesWithContributorRequest() {
        List<Profile> profiles = profileRepository.findAll();
        return new ResponseEntity<>(profiles, HttpStatus.OK);
    }

    /*
     * Returns the profile id of the profile in the database.
     */
    @Operation(summary = "Returns the profile id of the profile with the given email in the header.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned the id of the profile."),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Exercise not found.",
                    content = @Content)})
    @GetMapping
    public ResponseEntity<ProfileId> getReferenceToProfile(@RequestHeader String email) {
        Profile relatedProfile = profileRepository.getByEmail(email);
        if (relatedProfile == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ProfileId profileId = new ProfileId();
        profileId.setProfileId(relatedProfile.getProfile_id());
        return new ResponseEntity<>(profileId, HttpStatus.SEE_OTHER);
    }

    /*
     * Returns information on the profile.
     */
    @Operation(summary = "Get a profile by its id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned the exercise."),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Exercise not found.",
                    content = @Content)})
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<Profile> getProfile(@PathVariable(value = "id") long id) {
        if (id <= 0)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        if (!profileRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(profileRepository.findById(id).get(), HttpStatus.OK);
    }

    /*
     * Create a profile in the database
     */
    @Operation(summary = "Create a new profile in the database.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created the profile."),
            @ApiResponse(responseCode = "400", description = "Invalid profile supplied.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to create content.",
                    content = @Content)})
    @PostMapping
    public ResponseEntity<Profile> createProfile(@RequestBody Profile profile) {
        return new ResponseEntity<>(profileRepository.save(profile), HttpStatus.CREATED);
    }

    @Operation(summary = "Get all the goals connected to a profile")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned the goals of the profile."),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Profile with given id not found.",
                    content = @Content)})
    @GetMapping(value = "/{id}/goals")
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<List<Goal>> getGoalsFromProfile(@PathVariable(value = "id") long id) {
        if (id <= 0)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        if (!profileRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Profile profile = profileRepository.findById(id).get();
        return new ResponseEntity<>(profile.getUserGoals(), HttpStatus.OK);
    }

    @Operation(summary = "Get all the goals connected to a profile between two dates.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned the goals between the two dates of the profile."),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Profile with given id not found.",
                    content = @Content)})
    @GetMapping(value = "/{id}/goals/between-dates")
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<List<Goal>> getGoalsBetweenDatesFromProfile(@PathVariable(value = "id") long id,
                                                                      @RequestHeader Date StartDate,
                                                                      @RequestHeader Date EndDate) {
        if (id <= 0)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        if (!profileRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        Profile profile = profileRepository.findById(id).get();
        List<Goal> goalList = profile.getUserGoals();
        List<Goal> correctlyDatedGoals = new ArrayList<>();
        for (Goal goal : goalList) {
            if (goal.getStart_date().equals(StartDate) || goal.getEnd_date().equals(EndDate)) {
                correctlyDatedGoals.add(goal);
            }
        }
        return new ResponseEntity<>(correctlyDatedGoals, HttpStatus.OK);
    }

    @Operation(summary = "Delete an existing profile in the database.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Deleted the profile."),
            @ApiResponse(responseCode = "400", description = "Invalid profile_id supplied.",
                    content = @Content),
            @ApiResponse(responseCode = "403", description = "Not allowed to delete profile.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Could not find a profile with the given id.",
                    content = @Content),})
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<Profile> deleteProfile(@PathVariable(value = "id") long id) {
        if (id <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (!profileRepository.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Profile profile = profileRepository.findById(id).get();
        for (Goal goal : profile.getUserGoals()) {
            goalController.deleteGoal(goal.getGoal_id());
        }

        profileRepository.delete(profile);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Operation(summary = "Update an existing profile in the database.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the profile."),
            @ApiResponse(responseCode = "401", description = "Not allowed to create content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Did not find profile with the given id.",
                    content = @Content)})
    @PutMapping
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<Profile> updateProfile(@RequestBody Profile profile) {
        if (!profileRepository.existsById(profile.getProfile_id())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Profile oldProfile = profileRepository.findById(profile.getProfile_id()).get();
        profile.setUserGoals(oldProfile.getUserGoals());
        return new ResponseEntity<>(profileRepository.save(profile), HttpStatus.OK);
    }
}

