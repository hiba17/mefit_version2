package com.experis.mefit.controllers;

import com.experis.mefit.models.Exercise;
import com.experis.mefit.repositories.ExerciseRepository;
import com.experis.mefit.repositories.ExerciseSetRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/exercise")
public class ExerciseController {

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Autowired
    private ExerciseSetRepository exerciseSetRepository;

    /*
     * Get exercise by id.
     */
    @Operation(summary = "Get an exercise by its id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the exercise.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Exercise.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Exercise not found.",
                    content = @Content)})
    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<Exercise> getExerciseById(@PathVariable(value = "id") long id) {
        if (id <= 0)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        if (!exerciseRepository.existsById(id))
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(exerciseRepository.findById(id).get(), HttpStatus.OK);
    }

    /*
     * Get all exercises.
     */
    @Operation(summary = "Get all exercises.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returned the exercises."),
            @ApiResponse(responseCode = "401", description = "Not allowed to view content.",
                    content = @Content)})
    @GetMapping
    @PreAuthorize("hasAuthority('read:data')")
    public ResponseEntity<List<Exercise>> getAllExercises() {
        List<Exercise> exerciseList = exerciseRepository.findAll();
        return new ResponseEntity<>(exerciseRepository.findAll(), HttpStatus.OK);
    }

    /*
     * Create new exercise.
     */
    @Operation(summary = "Create a new exercise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Created an exercise.",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Exercise.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid exercise submitted.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to create content.",
                    content = @Content)})
    @PostMapping
    @PreAuthorize("hasAuthority('create:data')")
    public ResponseEntity<Exercise> createExercise(@RequestBody Exercise exercise) {
        return new ResponseEntity<>(exerciseRepository.save(exercise), HttpStatus.CREATED);
    }

    /*
     * Delete an exercise
     */
    @Operation(summary = "Delete an exercise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Deleted an exercise.",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to delete content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Exercise with given exercise_id doesn't exist.",
                    content = @Content),
    })
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('create:data')")
    public ResponseEntity<HttpStatus> deleteExercise(@PathVariable(value = "id") long id) {
        if (id <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (!exerciseRepository.existsById(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Exercise exercise = exerciseRepository.findById(id).get();
        // Delete all sets containing this exercise
        exerciseSetRepository.deleteByExercise(exercise);
        // Delete this exercise
        exerciseRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @Operation(summary = "Update an exercise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Updated the exercise.",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "Not allowed to update content.",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Exercise with given exercise_id doesn't exist.",
                    content = @Content),})
    @PutMapping
    @PreAuthorize("hasAuthority('create:data')")
    public ResponseEntity<Exercise> updateExercise(@RequestBody Exercise exercise) {
        if (!exerciseRepository.existsById(exercise.getExercise_id())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(exerciseRepository.save(exercise), HttpStatus.OK);
    }
}
